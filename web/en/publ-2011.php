<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">2011</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">Anisotropic magnetotransport in SrTiO(3) surface electron gases generated
                        by Ar(+) irradiation</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Bruno, F. Y.; Tornos, J.; Gutierrez del
                        Olmo, M.; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 83 Issue: 24 Published: JUN
                        23 2011<br/><b>Times Cited</b> &nbsp; 5<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.83.245120
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.83.245120" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2011/BrunoetalPhysRevB832011p245120.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Chemically Driven Nanoscopic Magnetic Phase Separation at the
                        SrTiO(3)(001)/La(1-x)Sr(x)CoO(3) Interface</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Torija, Maria A.; Sharma, Manish; Gazquez,
                        Jaume; et al.<br/><b>Source</b> &nbsp; Advanced Materials Volume: 23 Issue: 24 Pages: 2711-2715
                        Published: JUN 24 2011<br/><b>Times Cited</b> &nbsp; 5<br/><b>DOI</b> &nbsp;
                        10.1002/adma.201100417
                    </div>
                    <a href="http://dx.doi.org/10.1002/adma.201100417" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a href="../../files/publications/2011/TorijaetalAdvMater232011p2711.pdf"
                                               class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Coercivity enhancement driven by interfacial magnetic phase separation in
                        SrTiO(3)(001)/Nd(0.5)Sr(0.5)CoO(3)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sharma, M.; Gazquez, J.; Varela, M.; et al.<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 84 Issue: 2 Published: JUL 13 2011<br/><b>Times Cited</b>
                        &nbsp; 5<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.84.024417
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.84.024417" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2011/SharmaetalPhysRevB842011p024417.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Density of states deduced from ESR measurements on low-dimensional
                        nanostructures; benchmarks to identify the ESR signals of graphene and SWCNTs</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Szirmai, Peter; Fabian, Gabor; Dora,
                        Balazs; et al.<br/><b>Source</b> &nbsp; Physica Status Solidi B-Basic Solid State Physics
                        Volume: 248 Issue: 11 Pages: 2688-2691 Published: NOV 2011<br/><b>Times Cited</b> &nbsp;
                        2<br/><b>DOI</b> &nbsp; 10.1002/pssb.201100191
                    </div>
                    <a href="http://dx.doi.org/10.1002/pssb.201100191" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2011/SzirmaietalPhysStatusSolidiB2482011p2688.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Electric transport measurements on
                        La(0.7)Ca(0.3)MnO(3)/YBa(2)Cu(3)O(7-delta) heterostructures</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Hu, T.; Xiao, H.; Visani, C.; et
                        al.<br/><b>Source</b> &nbsp; International Conference on Strongly Correlated Electron Systems
                        (Sces 2010) Volume: 273 Published: 2011<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp;
                        10.1088/1742-6596/273/1/012090
                    </div>
                    <a href="http://dx.doi.org/10.1088/1742-6596/273/1/012090" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2011/HuetalJPhysConfSeries2732011p012090.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Electronic and Magnetic Reconstructions in La(0.7)Sr(0.3)MnO(3)/SrTiO(3)
                        Heterostructures: A Case of Enhanced Interlayer Coupling Controlled by the Interface</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Bruno, F. Y.; Garcia-Barriocanal, J.;
                        Varela, M.; et al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 106 Issue: 14
                        Published: APR 8 2011<br/><b>Times Cited</b> &nbsp; 14<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevLett.106.147205
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevLett.106.147205" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2011/BrunoetalPhysRevLett1062011p147205.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Exotic magnetic anisotropy map in epitaxial La(0.7)Ca(0.3)MnO(3) films on
                        BaTiO(3)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Alberca, A.; Nemes, N. M.; Mompean, F. J.;
                        et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 84 Issue: 13 Published: OCT 4
                        2011<br/><b>Times Cited</b> &nbsp; 8<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.84.134402
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.84.134402" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2011/AlbercaetalPhysRevB842011p134402.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Growth temperature control of the epitaxy, magnetism, and transport in
                        SrTiO(3)(001)/La(0.5)Sr(0.5)CoO(3) thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sharma, M.; Gazquez, J.; Varela, M.; et al.<br/><b>Source</b>
                        &nbsp; Journal of Vacuum Science & Technology a Volume: 29 Issue: 5 Published: SEP 2011<br/><b>Times
                            Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp; 10.1116/1.3622621
                    </div>
                    <a href="http://dx.doi.org/10.1116/1.3622621" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">In-plane impedance spectroscopy in aerosol deposited NiMn(2)O(4) negative
                        temperature coefficient thermistor films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ryu, Jungho; Park, Dong-Soo; Schmidt,
                        Rainer<br/><b>Source</b> &nbsp; Journal of Applied Physics Volume: 109 Issue: 11 Published: JUN
                        1 2011<br/><b>Times Cited</b> &nbsp; 8<br/><b>DOI</b> &nbsp; 10.1063/1.3592300
                    </div>
                    <a href="http://dx.doi.org/10.1063/1.3592300" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a href="../../files/publications/2011/JRyuetalJApplPhys1092011p113722.pdf"
                                               class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Seeing oxygen disorder in YSZ/SrTiO(3) colossal ionic conductor
                        heterostructures using EELS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pennycook, T. J.; Oxley, M. P.;
                        Garcia-Barriocanal, J.; et al.<br/><b>Source</b> &nbsp; European Physical Journal-Applied
                        Physics Volume: 54 Issue: 3 Published: JUN 2011<br/><b>Times Cited</b> &nbsp; 15<br/><b>DOI</b>
                        &nbsp; 10.1051/epjap/2011100413
                    </div>
                    <a href="http://dx.doi.org/10.1051/epjap/2011100413" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2011/PennycooketalEurPhysJApplPhys542011p33507.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Stray field and the superconducting surface spin valve effect in
                        La(0.7)Ca(0.3)MnO(3)/YBa(2)Cu(3)O(7-delta) bilayers</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Hu, T.; Xiao, H.; Visani, C.; et
                        al.<br/><b>Source</b> &nbsp; New Journal of Physics Volume: 13 Published: MAR 31 2011<br/><b>Times
                            Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp; 10.1088/1367-2630/13/3/033040
                    </div>
                    <a href="http://dx.doi.org/10.1088/1367-2630/13/3/033040" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2011/HuetalNewJPhys132011p033040.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Symmetrical interfacial reconstruction and magnetism in
                        La(0.7)Ca(0.3)MnO(3)/YBa(2)Cu(3)O(7)/La(0.7)Ca(0.3)MnO(3) heterostructures</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Visani, C.; Tornos, J.; Nemes, N. M.; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 84 Issue: 6 Published: AUG 23
                        2011<br/><b>Times Cited</b> &nbsp; 9<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.84.060405
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.84.060405" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2011/VisanietalPhysRevB842011p060405R.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Tailoring Interface Structure in Highly Strained YSZ/STO
                        Heterostructures</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera-Calzada, A.; Diaz-Guillen, M. R.;
                        Dura, O. J.; et al.<br/><b>Source</b> &nbsp; Advanced Materials Volume: 23 Issue: 44 Pages:
                        5268-5274 Published: NOV 23 2011<br/><b>Times Cited</b> &nbsp; 7<br/><b>DOI</b> &nbsp;
                        10.1002/adma.201102106<br/></div>
                    <a href="http://onlinelibrary.wiley.com/doi/10.1002/adma.201102106/abstract"
                       class="btn btn-default btn-sm" target="_blank">Link onlinelibrary.wiley.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1002/adma.201102106" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a href="../../files/publications/2011/RiveraCalzadaAdvMater232011p5268.pdf"
                                               class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Transport, electronic, and structural properties of nanocrystalline
                        CuAlO(2) delafossites</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Dura, O. J.; Boada, R.; Rivera-Calzada, A.;
                        et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 83 Issue: 4 Published: JAN 26
                        2011<br/><b>Times Cited</b> &nbsp; 10<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.83.045202
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.83.045202" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2011/DuraetalPhysRevB832011045202.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Upper Limit to Magnetism in LaAlO(3)/SrTiO(3) Heterostructures</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Fitzsimmons, M. R.; Hengartner, N. W.;
                        Singh, S.; et al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 107 Issue: 21
                        Published: NOV 14 2011<br/><b>Times Cited</b> &nbsp; 15<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevLett.107.217201
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevLett.107.217201" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2011/FitzsimmonsetalPhysRevLett1072011p217201.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>