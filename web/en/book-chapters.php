<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>Book Chapters</h1>
                    <hr class="small">
                    <span class="subheading">Physics of Complex Materials Group</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <div class="book-chapter-entry">
                <h4 class="paper-title">
                    Junko Habasaki, Carlos León y Kia Ngai &nbsp;
                    <span class="text-muted">(2016)</span>
                </h4>

                <div class="row add-margin-top">
                    <div class="col-md-4">
                        <img src="../../img/book-dynamics-glassy.png">
                    </div>
                    <div class="col-md-8">
                        <div class="well">
                            <p class="small-margin-p">
                                <em>Dynamics of Glassy, Crystalline and Liquid Ionic Conductors: Experiments, Theories, Simulations</em>
                            </p>

                            <p class="small-margin-p">
                                <span class="text-muted">
                                    Published by Springer-Nature
                                </span>
                            </p>

                            <p class="small-margin-p">
                                El libro revisa el trabajo más relevante sobre dinámica de iones móviles en conductores iónicos, tanto experimental como
                                teórico, intentando facilitar a los nuevos investigadores el acceso tanto a la historia de
                                este campo de investigación como a sus actuales fronteras. El libro también puede utilizarse
                                como material docente en cursos de grado y posgrado en instituciones académicas, dado que
                                cubre desde los fundamentos de los conductors iónicos hasta sus diversas aplicaciones en
                                distintos campos.
                            </p>
                        </div>
                        <a href="https://www.springer.com/us/book/9783319423890" class="btn btn-default btn-sm" target="_blank">
                            <i class="fa fa-external-link"></i> &nbsp; Link to springer.com
                        </a>
                    </div>
                </div>
            </div>

            <hr/>

            <div class="book-chapter-entry">
                <h4 class="paper-title">
                    R. Schmidt, J. Prado-Gonjal, D. Ávila, U. Amador, E. Morán &nbsp;
                    <span class="text-muted">(2014)</span>
                </h4>

                <div class="add-margin-top well">
                    <p class="small-margin-p">
                        <em>Electron microscopy of microwave-synthesized rare-earth chromites</em>
                    </p>

                    <p class="small-margin-p">
                        <span class="text-muted">
                            in
                            <em>Microscopy: advances in scientific research and education</em> Microscopy Book Series #6
                            <br/> Editor: A. Mendez-Vilas, Formatex Research Center, Badajoz (Spain), pp.819-826 pages
                        </span>
                    </p>

                    <p class="small-margin-p">
                        <b>ISBN</b> &nbsp;
                        <code>978-84-942134-4-1</code>
                    </p>
                </div>
            </div>

            <hr/>

            <div class="book-chapter-entry">
                <h4 class="paper-title">
                    R. Schmidt, E. Langenberg, J. Ventura &nbsp;
                    <span class="text-muted">(2013)</span>
                </h4>

                <div class="add-margin-top well">
                    <p class="small-margin-p">
                        <em>Bi-containing multiferroic perovskite oxide thin films</em>
                    </p>

                    <p class="small-margin-p">
                        <span class="text-muted">
                            in
                            <em>Perovskites - Crystallography, Chemistry and Catalytic Performance</em>
                            <br/> Editors: J. Zhang, H. Li, Novascience Publishers, Hauppauge, USA
                        </span>
                    </p>

                    <p class="small-margin-p">
                        <b>ISBN</b> &nbsp;
                        <code>978-1-62417-800-9</code>
                    </p>
                </div>

                <a href="../../files/book-chapters/SchmidtPerovskite2013.pdf" class="btn btn-default btn-sm" target="_blank">
                    <i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
            </div>

            <hr/>

            <div class="book-chapter-entry">
                <h4 class="paper-title">
                    J. Prado-Gonjal, R. Schmidt, E. Moran &nbsp;
                    <span class="text-muted">(2013)</span>
                </h4>

                <div class="add-margin-top well">
                    <p class="small-margin-p">
                        <em>Microwave-Assisted Synthesis and Characterization of Perovskite Oxides</em>
                    </p>

                    <p class="small-margin-p">
                        <span class="text-muted">
                            in
                            <em>Perovskites - Crystallography, Chemistry and Catalytic Performance</em>
                            <br/> Editors: J. Zhang, H. Li, Novascience Publishers, Hauppauge, USA
                        </span>
                    </p>

                    <p class="small-margin-p">
                        <b>ISBN</b> &nbsp;
                        <code>978-1-62417-800-9</code>
                    </p>
                </div>

                <a href="../../files/book-chapters/PradoGonjalPerovskite2013.pdf" class="btn btn-default btn-sm" target="_blank">
                    <i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
            </div>

            <hr/>

            <div class="book-chapter-entry">
                <h4 class="paper-title">
                    R. Schmidt, D.C. Sinclair &nbsp;
                    <span class="text-muted">(2013)</span>
                </h4>

                <div class="add-margin-top well">
                    <p class="small-margin-p">
                        <em>CaCu3Ti4O12 (CCTO) Ceramics for Capacitor Applications</em>
                    </p>

                    <p class="small-margin-p">
                        <span class="text-muted">
                            in
                            <em>Capacitors. Theory of Operation, Behavior and Safety Regulations</em>
                            <br/> Editor: K.N. Muller, Novascience Publishers, Hauppauge, USA
                        </span>
                    </p>

                    <p class="small-margin-p">
                        <b>ISBN</b> &nbsp;
                        <code>978-1-62417-586-2</code>
                    </p>
                </div>

                <a href="../../files/book-chapters/SchmidtCapacitors2013.pdf" class="btn btn-default btn-sm" target="_blank">
                    <i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
            </div>

            <hr/>

            <div class="book-chapter-entry">
                <h4 class="paper-title">
                    M.A. Frechero, M. Rocci, R. Schmidt, M.R. Diaz-Guillen, O.J. Dura, A. Rivera-Calzada, J. Santamaria, C. Leon &nbsp;
                    <span class="text-muted">(2012)</span>
                </h4>

                <div class="add-margin-top well">
                    <p class="small-margin-p">
                        <em>Efectos de interfase sobre la conductividad iónica en materiales de aplicación en pilas de combustible</em>
                    </p>

                    <p class="small-margin-p">
                        <span class="text-muted">
                            in
                            <em>Nanotecnología para energías en Latinoamérica</em>
                            <br/> Editor: E.M. Barea, Society for Nanomolecular Photovoltaics (SEFIN)
                        </span>
                    </p>

                    <p class="small-margin-p">
                        <b>ISBN</b> &nbsp;
                        <code>978-84-940189-9-2</code>
                    </p>
                </div>

                <a href="../../files/book-chapters/FrecheroNanotecnologia2012.pdf" class="btn btn-default btn-sm" target="_blank">
                    <i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
            </div>
        </div>
    </div>
</div>


<?php require_once("inc/footer.html") ?>