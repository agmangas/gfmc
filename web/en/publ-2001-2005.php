<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">2001 - 2005</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">Comparison of dynamics of ions in ionically conducting materials and
                        dynamics of glass-forming substances: Remarkable similarities</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ngai, KL; Habasaki, J; Leon, C; et al.<br/><b>Source</b>
                        &nbsp; Zeitschrift Fur Physikalische Chemie-International Journal of Research in Physical
                        Chemistry & Chemical Physics Volume: 219 Issue: 1 Pages: 47-70 Published: 2005<br/><b>Times
                            Cited</b> &nbsp; 42<br/><b>DOI</b> &nbsp; 10.1524/zpch.219.1.47.55017
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000226413300004"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1524/zpch.219.1.47.55017" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Cooperative oxygen ion dynamics in Gd2Ti2-yZryO7</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Moreno, KJ; Mendoza-Suarez, G; Fuentes, AF;
                        et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 71 Issue: 13 Published: APR
                        2005<br/><b>Times Cited</b> &nbsp; 29<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.71.132301
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000228761800006"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.71.132301" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Detailed structural analysis of epitaxial MBE-grown Fe/Cr superlattices by
                        x-ray diffraction and transmission-electron spectroscopy</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Gomez, ME; Santamaria, J; Kim, S; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 71 Issue: 12 Published: MAR 2005<br/><b>Times
                            Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.71.125410
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000228923300125"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.71.125410" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Effects of cooperativity on ion dynamics in oxygen conducting
                        Gd2Ti2-yZryO7</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Garcia-Barriocanal, J; Moreno, KJ;
                        Mendoza-Suarez, G; et al.<br/><b>Source</b> &nbsp; Journal of Non-Crystalline Solids Volume: 351
                        Issue: 33-36 Pages: 2813-2818 Published: SEP 15 2005<br/><b>Times Cited</b> &nbsp;
                        3<br/><b>DOI</b> &nbsp; 10.1016/j.jnoncrysol.2005.04.074
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000231993300038"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jnoncrysol.2005.04.074" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Effects of reduced dimensionality in the relaxation dynamics of ionic
                        conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Castro, M; Rivera, A; Garcia-Barriocanal,
                        J; et al.<br/><b>Source</b> &nbsp; Europhysics Letters Volume: 69 Issue: 5 Pages: 770-776
                        Published: MAR 2005<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp;
                        10.1209/epl/i2004-10419-7
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000227665100016"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1209/epl/i2004-10419-7" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Giant magnetoresistance in ferromagnet/superconductor superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V; Sefrioui, Z; Arias, D; et al.<br/><b>Source</b>
                        &nbsp; Physical Review Letters Volume: 94 Issue: 5 Published: FEB 11 2005<br/><b>Times Cited</b>
                        &nbsp; 121<br/><b>DOI</b> &nbsp; 10.1103/PhysRevLett.94.057002
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000226941500066"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevLett.94.057002" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Influence of vacancy ordering on the percolative behavior of
                        (Li1-xNax)(3y)La2/3-yTiO3 perovskites</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Herrero, CP; Varez, A; Rivera, A; et
                        al.<br/><b>Source</b> &nbsp; Journal of Physical Chemistry B Volume: 109 Issue: 8 Pages:
                        3262-3268 Published: MAR 3 2005<br/><b>Times Cited</b> &nbsp; 10<br/><b>DOI</b> &nbsp;
                        10.1021/jp046076p
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000227247100027"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1021/jp046076p" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Metal-insulator transition above room temperature in maximum colossal
                        magnetoresistance manganite thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Chen, XJ; Habermeier, HU; Zhang, H; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 72 Issue: 10 Published: SEP 2005<br/><b>Times
                            Cited</b> &nbsp; 32<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.72.104403
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000232228800043"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.72.104403" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Suppressed magnetization in La0.7Ca0.3MnO3/YBa2Cu3O7-delta
                        superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Hoffmann, A; te Velthuis, SGE; Sefrioui, Z;
                        et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 72 Issue: 14 Published: OCT
                        2005<br/><b>Times Cited</b> &nbsp; 45<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.72.140407
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000232934100011"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.72.140407" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Vortex phases in superconducting Nb thin films with periodic pinning</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Villegas, JE; Gonzalez, EM; Sefrioui, Z; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 72 Issue: 17 Published: NOV 2005<br/><b>Times
                            Cited</b> &nbsp; 15<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.72.174512
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000233603500109"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.72.174512" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Correlation between ion hopping conductivity and near constant loss in
                        ionic conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Ngai, KL; Rivera,
                        A<br/><b>Source</b> &nbsp; Physical Review B Volume: 69 Issue: 13 Published: APR 2004<br/><b>Times
                            Cited</b> &nbsp; 13<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.69.134303
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000221278100060"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.69.134303" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Coupling of superconductors through a half-metallic ferromagnet: Evidence
                        for a long-range proximity effect</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V; Sefrioui, Z; Arias, D; et al.<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 69 Issue: 22 Published: JUN 2004<br/><b>Times Cited</b> &nbsp;
                        103<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.69.224502
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000222530900064"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.69.224502" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Electrical conductivity and oxygen diffusion in Bifevox</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V; Rivera, A; Garcia-Barriocanal, J;
                        et al.<br/><b>Source</b> &nbsp; Boletin De La Sociedad Espanola De Ceramica Y Vidrio Volume: 43
                        Issue: 1 Pages: 67-70 Published: JAN-FEB 2004<br/><b>Times Cited</b> &nbsp; 1
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000220041900018"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Interface barriers for flux motion in high-temperature superconducting
                        superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Villegas, JE; Sefrioui, Z; Varela, M; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 69 Issue: 13 Published: APR 2004<br/><b>Times
                            Cited</b> &nbsp; 8<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.69.134505
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000221278100098"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.69.134505" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Long length scale interaction between magnetism and superconductivity in
                        La0.3Ca0.7MnO3/YBa2Cu3O7 superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V; Sefrioui, Z; Arias, D; et al.<br/><b>Source</b>
                        &nbsp; European Physical Journal B Volume: 40 Issue: 4 Pages: 479-482 Published: AUG
                        2004<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp; 10.1140/epjb/e2004-00230-x
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000224583500019"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1140/epjb/e2004-00230-x" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Oxygen mobility in A(2)Ti(2-y)Zr(y)O(7) (A : Y, Gd) ionic conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Moreno, KJ; Mendoza-Suarez, G; Fuentes, AF;
                        et al.<br/><b>Source</b> &nbsp; Boletin De La Sociedad Espanola De Ceramica Y Vidrio Volume: 43
                        Issue: 4 Pages: 759-763 Published: JUL-AUG 2004<br/><b>Times Cited</b> &nbsp; 4
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000223052500008"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Zero-magnetic-field dynamic scaling in Bi2Sr2CaCu2O8 thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; Arias, D; Leon, C; et al.<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 70 Issue: 6 Published: AUG 2004<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b>
                        &nbsp; 10.1103/PhysRevB.70.064502
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000223716300067"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.70.064502" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">A combined molecular dynamics simulation, experimental and coupling model
                        study of the ion dynamics in glassy ionic conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ngai, KL; Habasaki, J; Hiwatari, Y; et
                        al.<br/><b>Source</b> &nbsp; Journal of Physics-Condensed Matter Volume: 15 Issue: 16 Pages:
                        S1607-S1632 Published: APR 30 2003<br/><b>Times Cited</b> &nbsp; 29<br/><b>DOI</b> &nbsp;
                        10.1088/0953-8984/15/16/310
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000182936200011"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1088/0953-8984/15/16/310" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">A quantitative explanation of the difference between nuclear spin
                        relaxation and ionic conductivity relaxation in superionic glasses</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ngai, KL; Leon, C<br/><b>Source</b> &nbsp;
                        Journal of Non-Crystalline Solids Volume: 315 Issue: 1-2 Pages: 124-133 Published: JAN 2003<br/><b>Times
                            Cited</b> &nbsp; 19<br/><b>DOI</b> &nbsp; 10.1016/S0022-3093(02)01593-4
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000180165200015"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0022-3093(02)01593-4" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Comment on "Scaling of the interface roughness in Fe-Cr superlattices:
                        Self-affine versus non-self-affine" - Reply</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Santamaria, J; Gomez, ME; Vicent, JL; et
                        al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 91 Issue: 11 Published: SEP 12 2003<br/><b>Times
                            Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp; 10.1103/PhysRevLett.91.119602
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000185288000059"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevLett.91.119602" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Ferromagnetic/superconducting proximity effect in
                        La0.7Ca0.3MnO3/YBa2Cu3O7-delta superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; Arias, D; Pena, V; et al.<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 67 Issue: 21 Published: JUN 1 2003<br/><b>Times Cited</b>
                        &nbsp; 127<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.67.214511
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000184011100073"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.67.214511" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Influence of quenching treatments on structure and conductivity of the
                        Li3xLa2/3-xTiO3 series</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Varez, A; Ibarra, J; Rivera, A; et al.<br/><b>Source</b>
                        &nbsp; Chemistry of Materials Volume: 15 Issue: 1 Pages: 225-232 Published: JAN 14 2003<br/><b>Times
                            Cited</b> &nbsp; 25<br/><b>DOI</b> &nbsp; 10.1021/cm020172q
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000180368000034"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1021/cm020172q" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Li mobility in (Li,Na)(y)La0.66-y/3TiO3 perovskites (0.09 < y <= 0.5). A
                        model system for the percolation theory.</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sanz, J; Rivera, A; Leon, C; et al.<br/><b>Source</b>
                        &nbsp; Solid State Ionics-2002 Volume: 756 Pages: 51-56 Published: 2003<br/><b>Times Cited</b>
                        &nbsp; 1
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000183159100006"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Nanoscale analysis of YBa2Cu3O7-x/La0.67Ca0.33MnO3 interfaces</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Varela, M; Lupini, AR; Pennycook, SJ; et
                        al.<br/><b>Source</b> &nbsp; Solid-State Electronics Volume: 47 Issue: 12 Pages: 2245-2248
                        Published: DEC 2003<br/><b>Times Cited</b> &nbsp; 47<br/><b>DOI</b> &nbsp;
                        10.1016/S0038-1101(03)00205-3
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000186794900020"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0038-1101(03)00205-3" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Pair breaking by chain oxygen disorder in light-ion irradiated YBa2Cu3Ox
                        thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Arias, D; Sefrioui, Z; Loos, GD; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 68 Issue: 9 Published: SEP 1 2003<br/><b>Times
                            Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.68.094515
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000185717600096"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.68.094515" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Temperature dependence of the ionic conductivity in Li3xLa2/3-xTiO3:
                        Arrhenius versus non-Arrhenius</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera, A; Santamaria, J; Leon, C; et
                        al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 82 Issue: 15 Pages: 2425-2427
                        Published: APR 14 2003<br/><b>Times Cited</b> &nbsp; 11<br/><b>DOI</b> &nbsp; 10.1063/1.1568169
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000182104900019"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.1568169" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Temperature dependence of the near constant loss in ionic conductors: a
                        coupling model approach</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera, A; Santamaria, J; Leon, C; et
                        al.<br/><b>Source</b> &nbsp; Journal of Physics-Condensed Matter Volume: 15 Issue: 16 Pages:
                        S1633-S1642 Published: APR 30 2003<br/><b>Times Cited</b> &nbsp; 6<br/><b>DOI</b> &nbsp;
                        10.1088/0953-8984/15/16/311
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000182936200012"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1088/0953-8984/15/16/311" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Artificially induced reduction of the dissipation anisotropy in
                        high-temperature superconductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Gonzalez, EM; Villegas, JE; Varela, M; et
                        al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 80 Issue: 21 Pages: 3994-3996
                        Published: MAY 27 2002<br/><b>Times Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp; 10.1063/1.1482420
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000175709000038"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.1482420" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Cage decay, near constant loss, and crossover to cooperative ion motion in
                        ionic conductors: Insight from experimental data</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ngai, KL; Leon, C<br/><b>Source</b> &nbsp;
                        Physical Review B Volume: 66 Issue: 6 Published: AUG 1 2002<br/><b>Times Cited</b> &nbsp;
                        66<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.66.064308
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000177911700054"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.66.064308" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Cation mass dependence of the nearly constant dielectric loss in alkali
                        triborate glasses</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera, A; Leon, A; Varsamis, CPE; et
                        al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 88 Issue: 12 Published: MAR 25 2002<br/><b>Times
                            Cited</b> &nbsp; 28<br/><b>DOI</b> &nbsp; 10.1103/PhysRevLett.88.125902
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000174542000051"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevLett.88.125902" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Comment on "Ionic conduction in glass: New information on the interrelation
                        between the 'Jonscher behavior' and the 'Nearly constant-loss behavior' from broadband
                        conductivity spectra"</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Rivera, A; Santamaria, J; et
                        al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 89 Issue: 7 Published: AUG 12
                        2002<br/><b>Times Cited</b> &nbsp; 8<br/><b>DOI</b> &nbsp; 10.1103/PhysRevLett.89.079601
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000177200100055"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevLett.89.079601" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Correlated oxygen diffusion in BIFEVOX</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V; Rivera, A; Leon, C; et al.<br/><b>Source</b>
                        &nbsp; Chemistry of Materials Volume: 14 Issue: 4 Pages: 1606-1609 Published: APR 2002<br/><b>Times
                            Cited</b> &nbsp; 8<br/><b>DOI</b> &nbsp; 10.1021/cm010743z
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000175028700023"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1021/cm010743z" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Crossover from ionic hopping to nearly constant loss in the fast ionic
                        conductor Li0.18La0.61TiO3</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera, A; Leon, C; Sanz, J; et al.<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 65 Issue: 22 Published: JUN 1 2002<br/><b>Times Cited</b>
                        &nbsp; 17<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.65.224302
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000176767100056"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.65.224302" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Crossover of near-constant loss to ion hopping relaxation in ionically
                        conducting materials: experimental evidences and theoretical interpretation</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Rivera, A; Varez, A; et
                        al.<br/><b>Source</b> &nbsp; Journal of Non-Crystalline Solids Volume: 305 Issue: 1-3 Pages:
                        88-95 Published: JUL 1 2002<br/><b>Times Cited</b> &nbsp; 7<br/><b>DOI</b> &nbsp;
                        10.1016/S0022-3093(02)01126-2
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000176261600012"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0022-3093(02)01126-2" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Direct correlation between T-c and CuO2 bilayer spacing in YBa2Cu3O7-x</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Varela, M; Arias, D; Sefrioui, Z; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 66 Issue: 13 Published: OCT 1
                        2002<br/><b>Times Cited</b> &nbsp; 6<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.66.134517
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000179067900118"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.66.134517" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Direct growth of epitaxial La0.67Ca0.33MnO3-delta thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Campillo, G; Castro, LF; Vivas, P; et
                        al.<br/><b>Source</b> &nbsp; Surface Review and Letters Volume: 9 Issue: 5-6 Pages: 1611-1615
                        Published: OCT-DEC 2002<br/><b>Times Cited</b> &nbsp; 3<br/><b>DOI</b> &nbsp;
                        10.1142/S0218625X02004086
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000178447400002"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1142/S0218625X02004086" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Effects of epitaxial strain on the growth mechanism in YBa2Cu3O7-x thin
                        films in YBa2Cu3O7-x/PrBa2Cu3O7 superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Varela, M; Grogger, W; Arias, D; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 66 Issue: 17 Published: NOV 1
                        2002<br/><b>Times Cited</b> &nbsp; 11<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.66.174514
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000179611700098"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.66.174514" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Interfacially dominated giant magnetoresistance in Fe/Cr superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Santamaria, J; Gomez, ME; Cyrille, MC; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 65 Issue: 1 Published: JAN 1 2002<br/><b>Times
                            Cited</b> &nbsp; 11<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.65.012412
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000173186000030"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.65.012412" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Li3xLa(2/3)-xTiO3 fast ionic conductors. Correlation between lithium
                        mobility and structure</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera, A; Leon, C; Santamaria, J; et
                        al.<br/><b>Source</b> &nbsp; Journal of Non-Crystalline Solids Volume: 307 Pages: 992-998
                        Published: SEP 2002<br/><b>Times Cited</b> &nbsp; 15<br/><b>DOI</b> &nbsp;
                        10.1016/S0022-3093(02)01564-8
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000178038100133"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0022-3093(02)01564-8" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Origin and properties of the nearly constant loss in crystalline and glassy
                        ionic conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera, A; Santamaria, J; Leon, C; et
                        al.<br/><b>Source</b> &nbsp; Journal of Non-Crystalline Solids Volume: 307 Pages: 1024-1030
                        Published: SEP 2002<br/><b>Times Cited</b> &nbsp; 9<br/><b>DOI</b> &nbsp;
                        10.1016/S0022-3093(02)01568-5
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000178038100137"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0022-3093(02)01568-5" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Percolation-limited ionic diffusion in Li0.5-xNaxLa0.5TiO3 perovskites (0
                        <= x <= 0.5)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera, A; Leon, C; Santamaria, J; et
                        al.<br/><b>Source</b> &nbsp; Chemistry of Materials Volume: 14 Issue: 12 Pages: 5148-5152
                        Published: DEC 2002<br/><b>Times Cited</b> &nbsp; 34<br/><b>DOI</b> &nbsp; 10.1021/cm0204627
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000180016600037"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1021/cm0204627" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Scaling of the interface roughness in Fe-Cr superlattices: Self-affine
                        versus non-self-affine</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Santamaria, J; Gomez, ME; Vicent, JL; et
                        al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 89 Issue: 19 Published: NOV 4
                        2002<br/><b>Times Cited</b> &nbsp; 31<br/><b>DOI</b> &nbsp; 10.1103/PhysRevLett.89.190601
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000178790900007"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevLett.89.190601" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Superconductivity depression in ultrathin YBa2Cu3O7-delta layers in
                        La0.7Ca0.3MnO3/YBa2Cu3O7-delta superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; Varela, M; Pena, V; et
                        al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 81 Issue: 24 Pages: 4568-4570
                        Published: DEC 9 2002<br/><b>Times Cited</b> &nbsp; 74<br/><b>DOI</b> &nbsp; 10.1063/1.1526463
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000179611800024"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.1526463" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">The crossover from the near constant loss to ion hopping ac conductivity in
                        ionic conductors: the crossover times</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ngai, KL; Rendell, RW; Leon, C<br/><b>Source</b>
                        &nbsp; Journal of Non-Crystalline Solids Volume: 307 Pages: 1039-1049 Published: SEP
                        2002<br/><b>Times Cited</b> &nbsp; 15<br/><b>DOI</b> &nbsp; 10.1016/S0022-3093(02)01570-3
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000178038100139"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0022-3093(02)01570-3" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Chain oxygen disorder in deoxygenated YBa2Cu3O7-delta thin films induced by
                        light ion irradiation</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Arias, D; Sefrioui, Z; Varela, M; et
                        al.<br/><b>Source</b> &nbsp; Journal of Alloys and Compounds Volume: 323 Pages: 576-579
                        Published: JUL 12 2001<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp;
                        10.1016/S0925-8388(01)01180-X
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000169967300128"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0925-8388(01)01180-X" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Correlation between magnetic and transport properties in nanocrystalline Fe
                        thin films: A grain-boundary magnetic disorder effect</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; Menendez, JL; Navarro, E; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 64 Issue: 22 Published: DEC 1
                        2001<br/><b>Times Cited</b> &nbsp; 10<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.64.224431
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000172733300074"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.64.224431" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Direct evidence for block-by-block growth in high-temperature
                        superconductor ultrathin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Varela, M; Grogger, W; Arias, D; et
                        al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 86 Issue: 22 Pages: 5156-5159
                        Published: MAY 28 2001<br/><b>Times Cited</b> &nbsp; 24<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevLett.86.5156
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000169013600045"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevLett.86.5156" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Effect of anisotropy on the vortex liquid dissipation in YBa2Cu3O7-delta
                        thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; Arias, D; Varela, M; et
                        al.<br/><b>Source</b> &nbsp; Journal of Alloys and Compounds Volume: 323 Pages: 572-575
                        Published: JUL 12 2001<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp;
                        10.1016/S0925-8388(01)01179-3
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000169967300127"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0925-8388(01)01179-3" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Effect of light ion irradiation on the flux dynamics of YBa2Cu3O7-delta
                        thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Arias, D; Sefrioui, Z; Gonzalez, EM; et al.<br/><b>Source</b>
                        &nbsp; Ieee Transactions on Applied Superconductivity Volume: 11 Issue: 1 Pages: 3928-3930
                        Published: MAR 2001<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp; 10.1109/77.919932
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000168285900308"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1109/77.919932" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Electrical conductivity relaxation in thin-film yttria-stabilized
                        zirconia</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera, A; Santamaria, J; Leon, C<br/><b>Source</b>
                        &nbsp; Applied Physics Letters Volume: 78 Issue: 5 Pages: 610-612 Published: JAN 29 2001<br/><b>Times
                            Cited</b> &nbsp; 33<br/><b>DOI</b> &nbsp; 10.1063/1.1343852
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000166737500017"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.1343852" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Electron-electron interaction and weak localization effects in badly
                        metallic SrRuO3</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; de la Torre, MAL; Sefrioui, Z; Arias, D; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 63 Issue: 5 Published: FEB 1 2001<br/><b>Times
                            Cited</b> &nbsp; 13
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000166820600012"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Evidence for vortex tunnel dissipation in deoxygenated YBa2Cu3O6.4 thin
                        films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; Arias, D; Morales, F; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 63 Issue: 5 Published: FEB 1 2001<br/><b>Times
                            Cited</b> &nbsp; 5
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000166820600104"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">High-resolution and energy-filtered transmission electron microscopy of
                        YBa2Cu3O7-x/PrBa2Cu3O7 superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Varela, M; Ballesteros, C; Grogger, W; et
                        al.<br/><b>Source</b> &nbsp; Journal of Alloys and Compounds Volume: 323 Pages: 558-561
                        Published: JUL 12 2001<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp;
                        10.1016/S0925-8388(01)01143-4
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000169967300124"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0925-8388(01)01143-4" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Lithium intercalation in FeOC1 revisited</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sagua, A; Moran, E; Alario-Franco, MA; et
                        al.<br/><b>Source</b> &nbsp; International Journal of Inorganic Materials Volume: 3 Issue: 4-5
                        Pages: 293-301 Published: JUL 2001<br/><b>Times Cited</b> &nbsp; 7<br/><b>DOI</b> &nbsp;
                        10.1016/S1466-6049(01)00034-4
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000169816800003"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S1466-6049(01)00034-4" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Low temperature ac conductivity in the fast ionic conductor
                        Li0.18La0.61TiO3</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera, A; Varez, A; Sanz, J; et
                        al.<br/><b>Source</b> &nbsp; Journal of Alloys and Compounds Volume: 323 Pages: 545-548
                        Published: JUL 12 2001<br/><b>Times Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp;
                        10.1016/S0925-8388(01)01140-9
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000169967300121"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0925-8388(01)01140-9" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Magnetism and superconductivity in La0.7Ca0.3MnO3/YBa2Cu3O7-delta
                        superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Prieto, P; Vivas, P; Campillo, G; et
                        al.<br/><b>Source</b> &nbsp; Journal of Applied Physics Volume: 89 Issue: 12 Pages: 8026-8029
                        Published: JUN 15 2001<br/><b>Times Cited</b> &nbsp; 50<br/><b>DOI</b> &nbsp; 10.1063/1.1370994
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000169183500054"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.1370994" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Na-Li exchange of Na1+xTi2-xAlx(PO4)(3) (0.6 <= x <= 0.9) NASICON series: a
                        Rietveld and impedance study</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Mouahid, FE; Zahir, M; Maldonado-Manso, P;
                        et al.<br/><b>Source</b> &nbsp; Journal of Materials Chemistry Volume: 11 Issue: 12 Pages:
                        3258-3263 Published: 2001<br/><b>Times Cited</b> &nbsp; 21<br/><b>DOI</b> &nbsp;
                        10.1039/b102918p
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000172664200073"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1039/b102918p" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Nature and properties of the Johari-Goldstein beta-relaxation in the
                        equilibrium liquid state of a class of glass-formers</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ngai, KL; Lunkenheimer, P; Leon, C; et
                        al.<br/><b>Source</b> &nbsp; Journal of Chemical Physics Volume: 115 Issue: 3 Pages: 1405-1413
                        Published: JUL 15 2001<br/><b>Times Cited</b> &nbsp; 172<br/><b>DOI</b> &nbsp; 10.1063/1.1381054
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000169776100029"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.1381054" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Origin of constant loss in ionic conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Rivera, A; Varez, A; et
                        al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 86 Issue: 7 Pages: 1279-1282
                        Published: FEB 12 2001<br/><b>Times Cited</b> &nbsp; 135<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevLett.86.1279
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000166907600034"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevLett.86.1279" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Test of universal scaling of ac conductivity in ionic conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Lunkenheimer, P; Ngai, KL<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 64 Issue: 18 Published: NOV 1 2001<br/><b>Times Cited</b>
                        &nbsp; 17<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.64.184304
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000172239400051"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.64.184304" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Vortex liquid entanglement in irradiated YBa2Cu3O7 thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; Arias, D; Gonzalez, EM; et al.<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 63 Issue: 6 Published: FEB 1 2001<br/><b>Times Cited</b> &nbsp;
                        15<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.63.064503
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000166911400073"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.63.064503" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>