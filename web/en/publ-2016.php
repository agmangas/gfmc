<?php require_once "inc/header.html";?>
<?php require_once "inc/navigation.html";?>

<?php

$papers = array(
    array(
        "title" => "Landau modeling of dynamical nucleation of martensite at grain boundaries under local stress Computational study of atomic mobility in hcp Mg–Al–Zn ternary alloys",
        "fields" => array(
            "authors" => "J. Wang, N. Li, C.Wang, J. I. Beltran, J. Llorca and Y. Cui",
            "source" => "Calphad  54  134-143  2016. doi: 10.1016/j.calphad.2016.07.003",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Neutral Mononuclear Copper(I) Complexes: Synthesis, Crystal Structures, and Photophysical Properties",
        "fields" => array(
            "authors" => "Z. Sun, V. Lemaur, J.I. Beltrán, J. Cornil, J. Huang, J. Zhu, Y. Wang, R.Fröhlich, H.Wang., L.Jiang, and G. Zou.",
            "source" => "Inorganic Chemistry  55  12  5845-5852  2016. doi: 10.1021/acs.inorgchem.6b00101",
        ),
        "links" => array(),
    ),
    array(
        "title" => "TCNQ Grown on Cu (001): Its Atomic and Electronic Structure Determination",
        "fields" => array(
            "authors" => "M. J. Capitán, C. Navío, J. I. Beltrán, R. Otero, and J. Álvarez.",
            "source" => "The Journal of Physical Chemistry C  120  47  26889-26898  2016. doi: 10.1021/acs.jpcc.6b08999",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Tight-binding approach to penta-graphene",
        "fields" => array(
            "authors" => "T. Stauber, J. I. Beltrán & J. Schliemann",
            "source" => "Scientific Reports  6  22672  2016. doi: 10.1038/srep22672",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Diketonylpyridinium cations as a support of new ionic liquid crystals and ion-conductive materials: analysis of counter-ion effects",
        "fields" => array(
            "authors" => "M.J. Pastor, C. Cuerva, J.A. Campo, R. Schmidt, M. Rosario Torres, M. Cano",
            "source" => "Materials 9, p.360, 2016",
        ),
        "links" => array(),
    ),
    array(
        "title" => "The role of defects in microwave and conventionally synthesized LaCoO3 perovskite",
        "fields" => array(
            "authors" => "J. Prado Gonjal, J. Gutiérrez-Seijas, I. Herrero Ansorregui, E. Morán, I. Terry, R. Schmidt",
            "source" => " Journal of the European Ceramic Society 36, p.1197, 2016",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Nanostructured Bi2Te3 prepared by a straight-forward arc-melting method.",
        "fields" => array(
            "authors" => "M. Gharsallah, F. Serrano-Sánchez, J. Bermúdez, N.M. Nemes, J.L. Martínez, F. Elhalouani, J.A. Alonso",
            "source" => "Nanoscale research letters 11 (1)",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Strong enhancement of superconductivity at high pressures within the charge-density-wave states of 2H-TaS2 and 2H-TaSe2",
        "fields" => array(
            "authors" => "D. C. Freitas, P. Rodière, M. R. Osorio, E. Navarro-Moratalla, N. M. Nemes, V. G. Tissen, L. Cario, E. Coronado, M. García-Hernández, S. Vieira, M. Núñez-Regueiro, and H. Suderow",
            "source" => "Phys. Rev. B 93, 184512 (2016)",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Giant Seebeck effect in nanostructured Ge-doped SnSe",
        "fields" => array(
            "authors" => "M. Gharsallah, F. Serrano-Sánchez, N.M. Nemes, F.J. Mompeán, J.L. Martínez, M.T. Fernández-Díaz, F. Elhalouani, J.A. Alonso",
            "source" => "Scientific Reports 6, 26774 (2016)",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Cyan titania nanowires: Spectroscopic study of the origin of the self-doping enhanced photocatalytic activity",
        "fields" => array(
            "authors" => "Péter Szirmai, Bálint Náfrádi, Alla Arakcheeva, Edit Szilágyi, Richárd Gaál, Norbert M Nemes, Xavier Berdat, Massimo Spina, Laurent Bernard, Jaćim Jaćimović, Arnaud Magrez, László Forró, Endre Horváth",
            "source" => "Catalysis Today, 284, 52-58, 2016. doi: 10.1016/j.cattod.2016.10.024",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Structural phase transitions in polycrystalline SnSe: a neutron diffractionstudy in correlation with thermoelectric properties",
        "fields" => array(
            "authors" => "F. Serrano-Sánchez, J. Bermúdez, N. M. Nemes, O. J. Dura, M.T. Fernández-Díaz, J.L. Martínez, J.A. Alonso",
            "source" => "J. Appl. Cryst. 49, (6) 2138-2144, 2016. doi:10.1107/S1600576716015405",
        ),
        "links" => array(),
    ),
);

?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>Publications</h1>
                    <hr class="small">
                    <span class="subheading">2016</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?php foreach ($papers as $paper): ?>
            <div>
                <h4 class="paper-title">
                    <?php echo $paper["title"] ?>
                </h4>

                <div class="well add-margin-top">
                    <?php foreach ($paper["fields"] as $key => $val): ?>
                    <b>
                        <?php echo ucwords($key) ?>
                    </b> &nbsp;
                    <?php echo $val ?>
                    <br/>
                    <?php endforeach;?>
                </div>

                <?php foreach ($paper["links"] as $link): ?>
                <a href="<?php echo $link["url"] ?>" class="btn btn-default btn-sm" target="_blank">
                    Link
                    <?php echo $link["name"] ?>
                </a>
                <?php endforeach;?>
            </div>
            <hr/>
            <?php endforeach;?>

            <div>
                <h4 class="paper-title">
                    Induced Ti magnetization at La0. 7Sr0. 3MnO3 and BaTiO3 interfaces
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; Yaohua Liu, J Tornos, SGE te Velthuis, JW Freeland, Hua Zhou, P Steadman, P Bencok,
                    C Leon, J Santamaria
                    <br/>
                    <b>Source</b> &nbsp; APL Materials 4 (4), 046105
                    <br/>
                </div>

                <a href="http://aip.scitation.org/doi/10.1063/1.4946756" class="btn btn-default btn-sm" target="_blank">
                    Link aip.scitation.org
                </a>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    Water-Free Proton Conduction in Discotic Pyridylpyrazolate-based Pt(II) and Pd(II) Metallomesogens
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; C. Cuerva, J. A. Campo, M. Cano, J. Sanz, Isabel Sobrados, V. Diez-Gómez, A. Rivera-Calzada,
                    and R. Schmidt
                    <br/>
                    <b>Source</b> &nbsp; Inorg. Chem., 2016, 55 (14), pp 6995–7002
                    <br/>
                </div>

                <a href="http://pubs.acs.org/doi/abs/10.1021/acs.inorgchem.6b00728" class="btn btn-default btn-sm" target="_blank">
                    Link pubs.acs.org
                </a>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    In operando evidence of deoxygenation in ionic liquid gating of YBa2Cu3O7-X
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; A. M. Perez-Muñoz, P. Schio, R. Poloni, A. Fernandez-Martinez,A . Rivera-Calzada,
                    J. C. Cezar, E. Salas-Colera, G. R. Castro, J. Kinney, C. Leon, J. Santamaria, J. Garcia-Barriocanal,
                    and A. M. Goldman
                    <br/>
                    <b>Source</b> &nbsp; Proc. Natl. Acad. Sci. USA 114, 215 (2017)
                    <br/>
                    <b>DOI</b> &nbsp; 10.1073/pnas.1613006114
                    <br/>
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    High On/Off Ratio Memristive Switching of Manganite/Cuprate Bilayer by Interfacial Magnetoelectricity
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; X. Shen, T. Pennycook, D. Hernandez-Martin, A. Pérez, Y. Puzyrev, Y. Liu, S. te
                    Velthuis, J. Freeland, P. Shafer, C. Zhu, M. Varela, C. Leon, Z. Sefrioui, J. Santamaria, S. Pantelides
                    <br/>
                    <b>Source</b> &nbsp; Advanced Materials Interfaces 3 1600086 (2016)
                    <br/>
                    <b>DOI</b> &nbsp; 10.1002/admi.201600086
                    <br/>
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    On the low- to high proton-conducting transformation of a CsHSO4–CsH2PO4 solid solution and its parents: Physical or chemical
                    nature?
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; E. Ortiz, I. Piñeres, C. León
                    <br/>
                    <b>Source</b> &nbsp; J. Therm. Anal. Calorim. 126, 407 (2016)
                    <br/>
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    Magnetically controlled space charge capacitance at La1−xSrxMnO3 / SrxLa1−xTiO3 interfaces
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; R. Schmidt, J. García-Barriocanal, M. Varela, M. García-Hernández, C. León, J. Santamaría
                    <br/>
                    <b>Source</b> &nbsp; Phys. Status Solidi A, 213 2243 (2016)
                    <br/>
                    <b>DOI</b> &nbsp; 10.1002/pssa.201533036
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once "inc/footer.html"?>