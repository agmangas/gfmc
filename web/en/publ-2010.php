<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">2010</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">"Charge Leakage" at LaMnO(3)/SrTiO(3) Interfaces</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Garcia-Barriocanal, Javier; Bruno, Flavio
                        Y.; Rivera-Calzada, Alberto; et al.<br/><b>Source</b> &nbsp; Advanced Materials Volume: 22
                        Issue: 5 Pages: 627-+ Published: FEB 2 2010<br/><b>Times Cited</b> &nbsp; 33<br/><b>DOI</b>
                        &nbsp; 10.1002/adma.200902263
                    </div>
                    <a href="http://dx.doi.org/10.1002/adma.200902263" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2010/GarciaBarriocanaletalAdvMater222010p627.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">A Relationship between Intermolecular Potential, Thermodynamics, and
                        Dynamic Scaling for a Supercooled Ionic Liquid</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Paluch, M.; Haracz, S.; Grzybowski, A.; et
                        al.<br/><b>Source</b> &nbsp; Journal of Physical Chemistry Letters Volume: 1 Issue: 6 Pages:
                        987-992 Published: MAR 18 2010<br/><b>Times Cited</b> &nbsp; 24<br/><b>DOI</b> &nbsp;
                        10.1021/jz9004653
                    </div>
                    <a href="http://dx.doi.org/10.1021/jz9004653" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a href="../../files/publications/2010/PaluchetalJPhysChemLett12010p987.pdf"
                                               class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">All-Manganite Tunnel Junctions with Interface-Induced Barrier
                        Magnetism</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z.; Visani, C.; Calderon, M. J.;
                        et al.<br/><b>Source</b> &nbsp; Advanced Materials Volume: 22 Issue: 44 Pages: 5029-+ Published:
                        NOV 24 2010<br/><b>Times Cited</b> &nbsp; 12<br/><b>DOI</b> &nbsp; 10.1002/adma.201002067
                    </div>
                    <a href="http://dx.doi.org/10.1002/adma.201002067" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a href="../../files/publications/2010/SefriouietalAdvMater222010p5029.pdf"
                                               class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Anomalous Increase of Dielectric Permittivity in Sr-Doped CCTO Ceramics
                        Ca(1-x)Sr(x)Cu(3)Ti(4)O(12) (0 <= x <= 0. 2)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Schmidt, Rainer; Sinclair, Derek C.<br/><b>Source</b>
                        &nbsp; Chemistry of Materials Volume: 22 Issue: 1 Pages: 6-8 Published: JAN 12 2010<br/><b>Times
                            Cited</b> &nbsp; 21<br/><b>DOI</b> &nbsp; 10.1021/cm903220z<br/><br/></div>
                    <a href="http://dx.doi.org/10.1021/cm903220z" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2010/RSchmidtDCSinclairChemMater222010p6.pdf"
                        class="btn btn-default" target="_blank">PDF</a>&nbsp;<a
                        href="../../files/publications/2010/RSchmidtDCSinclairChemMater222010p6supportinginformation.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Crossover to nearly constant loss in ac conductivity of highly disordered
                        pyrochlore-type ionic conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Diaz-Guillen, M. R.; Diaz-Guillen, J. A.;
                        Fuentes, A. F.; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 82 Issue: 17
                        Published: NOV 18 2010<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevB.82.174304
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.82.174304" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2010/DiazGuillenetalPhysRevB822010p174304.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Directionally controlled superconductivity in
                        ferromagnet/superconductor/ferromagnet trilayers with biaxial easy axes</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Visani, C.; Nemes, N. M.; Rocci, M.; et al.<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 81 Issue: 9 Published: MAR 1 2010<br/><b>Times Cited</b> &nbsp;
                        10<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.81.094512
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.81.094512" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2010/VisanietalPhysRevB812010p094512.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Exchange-bias-modulated inverse superconducting spin switch in
                        CoO/Co/YBa(2)Cu(3)O(7-delta)/La(0.7)Ca(0.3)MnO(3) thin film hybrids</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Nemes, N. M.; Visani, C.; Sefrioui, Z.; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 81 Issue: 2 Published: JAN 2010<br/><b>Times
                            Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.81.024512
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.81.024512" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2010/NemesetalPhysRevB812010p024512.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Investigation of hydrogenated HiPCo nanotubes by infrared spectroscopy</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Nemeth, Katalin; Pekker, Aron; Borondics,
                        Ferenc; et al.<br/><b>Source</b> &nbsp; Physica Status Solidi B-Basic Solid State Physics
                        Volume: 247 Issue: 11-12 Pages: 2855-2858 Published: DEC 2010<br/><b>Times Cited</b> &nbsp;
                        0<br/><b>DOI</b> &nbsp; 10.1002/pssb.201000329
                    </div>
                    <a href="http://dx.doi.org/10.1002/pssb.201000329" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2010/NemethetalPhysStatusSolidiB2472010p2855.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Ionic conductivity of nanocrystalline yttria-stabilized zirconia: Grain
                        boundary and size effects</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Dura, O. J.; Lopez de la Torre, M. A.;
                        Vazquez, L.; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 81 Issue: 18 Published:
                        MAY 1 2010<br/><b>Times Cited</b> &nbsp; 15<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.81.184301
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.81.184301" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2010/DuraetalPhysRevB812010p184301.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Magnetic memory based on
                        La(0.7)Ca(0.3)MnO(3)/YBa(2)Cu(3)O(7)/La(0.7)Ca(0.3)MnO(3) ferromagnet/superconductor hybrid
                        structures</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Nemes, N. M.; Visani, C.; Leon, C.; et
                        al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 97 Issue: 3 Published: JUL 19
                        2010<br/><b>Times Cited</b> &nbsp; 6<br/><b>DOI</b> &nbsp; 10.1063/1.3464960
                    </div>
                    <a href="http://dx.doi.org/10.1063/1.3464960" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2010/NemesetalApplPhysLett972010p032501.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Spin and orbital Ti magnetism at LaMnO(3)/SrTiO(3) interfaces</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Garcia-Barriocanal, J.; Cezar, J. C.;
                        Bruno, F. Y.; et al.<br/><b>Source</b> &nbsp; Nature Communications Volume: 1 Published: SEP
                        2010<br/><b>Times Cited</b> &nbsp; 31<br/><b>DOI</b> &nbsp; 10.1038/ncomms1080
                    </div>
                    <a href="http://dx.doi.org/10.1038/ncomms1080" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Structure and physical properties of nickel manganite NiMn(2)O(4) obtained
                        from nickel permanganate precursor</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Diez, Alejandra; Schmidt, Rainer; Sagua,
                        Aurora E.; et al.<br/><b>Source</b> &nbsp; Journal of the European Ceramic Society Volume: 30
                        Issue: 12 Pages: 2617-2624 Published: SEP 2010<br/><b>Times Cited</b> &nbsp; 13<br/><b>DOI</b>
                        &nbsp; 10.1016/j.jeurceramsoc.2010.04.032
                    </div>
                    <a href="http://dx.doi.org/10.1016/j.jeurceramsoc.2010.04.032" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2010/ADiezetalJEurCeramSoc302010p2617.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Synthesis of Li(1+x)M(x)(III)Ti(2-x)(PO(4))(3) with nasicon structure,
                        using sol-gel methods. Study of the relationship microstructure-electrical properties</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Perez-Estebanez, M.; Rivera-Calzada, A.;
                        Leon, C.; et al.<br/><b>Source</b> &nbsp; Boletin De La Sociedad Espanola De Ceramica Y Vidrio
                        Volume: 49 Issue: 1 Pages: 41-46 Published: JAN-FEB 2010<br/><b>Times Cited</b> &nbsp; 1
                    </div>
                    <a href="../../files/publications/2010/PerezEstebanezetalBolSocEspCeramVidrio492010p41.pdf"
                       class="btn btn-default" target="_blank">PDF</a></div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>