<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">1996 - 2000</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">Critical temperature depression and persistent photoconductivity in ion
                        irradiated YBa2Cu3O7-x films and YBa2Cu3O7-x/PrBa2Cu3O7 superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Navacerrada, MA; Arias, D; Sefrioui, Z; et
                        al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 76 Issue: 22 Pages: 3289-3291
                        Published: MAY 29 2000<br/><b>Times Cited</b> &nbsp; 6<br/><b>DOI</b> &nbsp; 10.1063/1.126609
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000087239400044"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.126609" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Dissipation and anisotropy in ultrathin YBa Cu O /PrBa Cu O
                          superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Villegas, J.E.; Gonzalez, E.M.; Vicent,
                        J.L.; et al.<br/><b>Source</b> &nbsp; High-Temperature Superconductors - Crystal Chemistry,
                        Processing and Properties. Symposium (Materials Research Society Symposium Proceedings Vol.659)
                        Pages: II10.4.1-4 Published: 2001
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=INSPEC&amp;KeyUT=7613067"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Effect of silver doping on the transport properties of epitaxial
                        YBa2Cu3O7-x thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Bolanos, G; Prieto, P; Arias, D; et
                        al.<br/><b>Source</b> &nbsp; Physica C Volume: 341 Pages: 1197-1198 Published: NOV 2000<br/><b>Times
                            Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp; 10.1016/S0921-4534(00)00857-1
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000165855700198"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0921-4534(00)00857-1" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Effect of sputtering pressure-induced roughness on the microstructure and
                        the perpendicular giant magnetoresistance of Fe/Cr superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Cyrille, MC; Kim, S; Gomez, ME; et al.<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 62 Issue: 22 Pages: 15079-15083 Published: DEC 1 2000<br/><b>Times
                            Cited</b> &nbsp; 18<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.62.15079
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000165883800063"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.62.15079" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Enhancement of perpendicular and parallel giant magnetoresistance with the
                        number of bilayers in Fe/Cr superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Cyrille, MC; Kim, S; Gomez, ME; et al.<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 62 Issue: 5 Pages: 3361-3367 Published: AUG 1 2000<br/><b>Times
                            Cited</b> &nbsp; 29<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.62.3361
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000088714200069"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.62.3361" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Epitaxial mismatch strain in YBa2Cu3O7-delta/PrBa2Cu3O7 superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Varela, M; Arias, D; Sefrioui, Z; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 62 Issue: 18 Pages: 12509-12515
                        Published: NOV 1 2000<br/><b>Times Cited</b> &nbsp; 15<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevB.62.12509
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000165224500084"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.62.12509" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">He irradiation and oxygen disorder in epitaxial YBa Cu  O  thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Arias, D.; Sefrioui, Z.; Varela, M.; et al.<br/><b>Source</b>
                        &nbsp; Applied Superconductivity 1999. Proceedings of EUCAS 1999, the Fourth European Conference
                        on Applied Superconductivity Pages: 815-18 vol.1 Published: 2000
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=INSPEC&amp;KeyUT=7300037"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Influence of composition on the structure and conductivity of the fast
                        ionic conductors La2/3-xLi3xTiO3 (0.03 <= x <= 0.167)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ibarra, J; Varez, A; Leon, C; et
                        al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 134 Issue: 3-4 Pages: 219-228 Published:
                        OCT 2000<br/><b>Times Cited</b> &nbsp; 80<br/><b>DOI</b> &nbsp; 10.1016/S0167-2738(00)00761-X
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000165476200002"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0167-2738(00)00761-X" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Li mobility in the orthorhombic Li0.18La0.61TiO3 perovskite studied by NMR
                        and impedance spectroscopies</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Paris, MA; Sanz, J; Leon, C; et al.<br/><b>Source</b>
                        &nbsp; Chemistry of Materials Volume: 12 Issue: 6 Pages: 1694-1701 Published: JUN 2000<br/><b>Times
                            Cited</b> &nbsp; 59<br/><b>DOI</b> &nbsp; 10.1021/cm9911159
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000087708400032"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1021/cm9911159" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Magnetic field induced change in the dimensionality of the vortex  glass
                        transition in YBa Cu O  thin  films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z.; Arias, D.; Varela, M.; et al.<br/><b>Source</b>
                        &nbsp; Applied Superconductivity 1999. Proceedings of EUCAS 1999, the Fourth European Conference
                        on Applied Superconductivity Pages: 827-30 vol.1 Published: 2000
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=INSPEC&amp;KeyUT=7300040"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">On the location of Li+ cations in the fast Li-cation conductor
                        La0.5Li0.5TiO3 perovskite</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Alonso, JA; Sanz, J; Santamaria, J; et
                        al.<br/><b>Source</b> &nbsp; Angewandte Chemie-International Edition Volume: 39 Issue: 3 Pages:
                        619-+ Published: 2000<br/><b>Times Cited</b> &nbsp; 81<br/><b>DOI</b> &nbsp;
                        10.1002/(SICI)1521-3773(20000204)39:3<619::AID-ANIE619>3.0.CO;2-O
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000085230900045"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1002/(SICI)1521-3773(20000204)39:3&lt;619::AID-ANIE619&gt;3.0.CO;2-O"
                        class="btn btn-default btn-sm" target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Relationship between crystal structure and Li+-conductivity in
                        La0.5Li0.5TiO3 perovskite</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Alonso, JA; Ibarra, J; Paris, MA; et
                        al.<br/><b>Source</b> &nbsp; New Materials For Batteries and Fuel Cells Volume: 575 Pages:
                        337-342 Published: 2000<br/><b>Times Cited</b> &nbsp; 2
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000088058800043"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Structure characterization of epitaxial strain relaxation in
                        YBa2CU3O7-x/PrBa2Cu3O7 superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Varela, M; Arias, D; Sefrioui, Z; et
                        al.<br/><b>Source</b> &nbsp; Recent Developments in Oxide and Metal Epitaxy-Theory and
                        Experiment Volume: 619 Pages: 185-190 Published: 2000<br/><b>Times Cited</b> &nbsp; 0
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000168247300024"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">alpha-NaFeO2: ionic conductivity and sodium extraction</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Blesa, MC; Moran, E; Leon, C; et
                        al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 126 Issue: 1-2 Pages: 81-87 Published:
                        NOV 1999<br/><b>Times Cited</b> &nbsp; 9<br/><b>DOI</b> &nbsp; 10.1016/S0167-2738(99)00145-9
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000083840400007"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0167-2738(99)00145-9" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Crossover from a three-dimensional to purely two-dimensional vortex-glass
                        transition in deoxygenated YBa(2)Cu(3)O(7-delta) thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; Arias, D; Varela, M; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 60 Issue: 22 Pages: 15423-15429
                        Published: DEC 1 1999<br/><b>Times Cited</b> &nbsp; 31<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevB.60.15423
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000084631600063"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.60.15423" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Disorder and damage effects in SrRuO(3) thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; de la Torre, MAL; Arias, D; et
                        al.<br/><b>Source</b> &nbsp; Physica B-Condensed Matter Volume: 259-61 Pages: 938-939 Published:
                        JAN 1999<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp; 10.1016/S0921-4526(98)00601-2
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000079315700405"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0921-4526(98)00601-2" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Electrical characterization of Si+ and Si+/P+ implanted N+PIn0.53Ga0.47As
                        junctions</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Blanco, MN; Redondo, E; Leon, C; et
                        al.<br/><b>Source</b> &nbsp; Journal of Materials Science-Materials in Electronics Volume: 10
                        Issue: 5-6 Pages: 425-428 Published: JUL 1999<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b>
                        &nbsp; 10.1023/A:1008918128149
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000081718300017"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1023/A:1008918128149" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Intracell changes in epitaxially strained YBa2Cu3O7-x ultrathin layers in
                        YBa2Cu3O7-x/PrBa2Cu3O7 superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Varela, M; Sefrioui, Z; Arias, D; et
                        al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 83 Issue: 19 Pages: 3936-3939
                        Published: NOV 8 1999<br/><b>Times Cited</b> &nbsp; 56<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevLett.83.3936
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000083546200048"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevLett.83.3936" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Pure 2D vortex-glass phase transition with T-g=0 K in deoxygenated
                        YBa2Cu3O6.4 thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; Arias, D; Varela, M; et
                        al.<br/><b>Source</b> &nbsp; Europhysics Letters Volume: 48 Issue: 6 Pages: 679-685 Published:
                        DEC 1999<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp; 10.1209/epl/i1999-00538-7
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000084211100013"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1209/epl/i1999-00538-7" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Rapidity of the change of the Kohlrausch exponent of the alpha-relaxation
                        of glass-forming liquids at T-B or T-beta and consequences</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Ngai, KL<br/><b>Source</b> &nbsp;
                        Journal of Physical Chemistry B Volume: 103 Issue: 20 Pages: 4045-4051 Published: MAY 20
                        1999<br/><b>Times Cited</b> &nbsp; 78<br/><b>DOI</b> &nbsp; 10.1021/jp983756h
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000080565100010"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1021/jp983756h" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Recent advances in relating macroscopic electrical relaxation data to
                        microscopic movements of the ions in ionically conducting materials</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ngai, KL; Leon, C<br/><b>Source</b> &nbsp;
                        Solid State Ionics Volume: 125 Issue: 1-4 Pages: 81-90 Published: OCT 1999<br/><b>Times
                            Cited</b> &nbsp; 31<br/><b>DOI</b> &nbsp; 10.1016/S0167-2738(99)00161-7
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000083122700011"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0167-2738(99)00161-7" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Relating macroscopic electrical relaxation to microscopic movements of the
                        ions in ionically conducting materials by theory and experiment</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ngai, KL; Leon, C<br/><b>Source</b> &nbsp;
                        Physical Review B Volume: 60 Issue: 13 Pages: 9396-9405 Published: OCT 1 1999<br/><b>Times
                            Cited</b> &nbsp; 75<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.60.9396
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000083079200047"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.60.9396" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Relationship between the primary and secondary dielectric relaxation
                        processes in propylene glycol and its oligomers</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Ngai, KL; Roland,
                        CM<br/><b>Source</b> &nbsp; Journal of Chemical Physics Volume: 110 Issue: 23 Pages: 11585-11591
                        Published: JUN 15 1999<br/><b>Times Cited</b> &nbsp; 155
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000080521200053"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Shallow junctions in p-In.53Ga.47As by ion implantation</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Blanco, MN; Redondo, E; Leon, C; et
                        al.<br/><b>Source</b> &nbsp; Nuclear Instruments & Methods in Physics Research Section B-Beam
                        Interactions With Materials and Atoms Volume: 147 Issue: 1-4 Pages: 166-170 Published: JAN
                        1999<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp; 10.1016/S0168-583X(98)00606-5
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000077846200029"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0168-583X(98)00606-5" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Size effects on the critical scaling laws in a-axis and c-axis oriented 123
                        thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Gonzalez, EM; Luna, ME; Sefrioui, Z; et al.<br/><b>Source</b>
                        &nbsp; Journal of Low Temperature Physics Volume: 117 Issue: 3-4 Pages: 675-679 Published: NOV
                        1999<br/><b>Times Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp; 10.1023/A:1022576707696
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000084373900083"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1023/A:1022576707696" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Dynamic transport in ionic conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Jonscher, AK; Leon, C; Santamaria,
                        J<br/><b>Source</b> &nbsp; Journal of Materials Science Volume: 33 Issue: 18 Pages: 4485-4490
                        Published: SEP 15 1998<br/><b>Times Cited</b> &nbsp; 3<br/><b>DOI</b> &nbsp;
                        10.1023/A:1004435830639
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000079546400002"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1023/A:1004435830639" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Metal-insulator transition in SrRuO3 induced by ion irradiation</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; Arias, D; Navacerrada, MA; et
                        al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 73 Issue: 23 Pages: 3375-3377
                        Published: DEC 7 1998<br/><b>Times Cited</b> &nbsp; 21<br/><b>DOI</b> &nbsp; 10.1063/1.122772
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000077432600019"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.122772" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Non-Debye conductivity relaxation in the non-Arrhenius Li0.5La0.5TiO3 fast
                        ionic conductor. A nuclear magnetic resonance and complex impedance study</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Santamaria, J; Paris, MA; et
                        al.<br/><b>Source</b> &nbsp; Journal of Non-Crystalline Solids Volume: 235 Pages: 753-760
                        Published: AUG 1998<br/><b>Times Cited</b> &nbsp; 26<br/><b>DOI</b> &nbsp;
                        10.1016/S0022-3093(98)00626-7
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000075838800124"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0022-3093(98)00626-7" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Study of the conductivity of Nax-delta FexTi2-xO4 (x = 0.875, 0 <=delta <=
                        0.40)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Kuhn, A; Leon, C; Garcia-Alvarado, F; et
                        al.<br/><b>Source</b> &nbsp; Journal of Solid State Chemistry Volume: 137 Issue: 1 Pages:
                        168-173 Published: APR 1998<br/><b>Times Cited</b> &nbsp; 3<br/><b>DOI</b> &nbsp;
                        10.1006/jssc.1998.7749
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000074006200023"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1006/jssc.1998.7749" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Universal scaling of the conductivity relaxation in crystalline ionic
                        conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Lucia, ML; Santamaria, J; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 57 Issue: 1 Pages: 41-44 Published: JAN 1
                        1998<br/><b>Times Cited</b> &nbsp; 31<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.57.41
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000071320800011"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.57.41" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Analytical distributions of relaxation times for the description of
                        electrical conductivity relaxation in ionic conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Lucia, ML; Santamaria, J<br/><b>Source</b>
                        &nbsp; Philosophical Magazine B-Physics of Condensed Matter Statistical Mechanics Electronic
                        Optical and Magnetic Properties Volume: 75 Issue: 5 Pages: 629-638 Published: MAY 1997<br/><b>Times
                            Cited</b> &nbsp; 14<br/><b>DOI</b> &nbsp; 10.1080/13642819708202344
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1997WX94300002"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1080/13642819708202344" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Correlated ion hopping in single-crystal yttria-stabilized zirconia</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Lucia, ML; Santamaria, J<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 55 Issue: 2 Pages: 882-887 Published: JAN 1 1997<br/><b>Times
                            Cited</b> &nbsp; 135<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.55.882
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1997WD78900037"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.55.882" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Lanthanum-lithium-sodium double chromates as ionic conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Melnikov, P; Leon, C; Santamara, J; et
                        al.<br/><b>Source</b> &nbsp; Journal of Alloys and Compounds Volume: 250 Issue: 1-2 Pages:
                        520-523 Published: MAR 20 1997<br/><b>Times Cited</b> &nbsp; 3<br/><b>DOI</b> &nbsp;
                        10.1016/S0925-8388(96)02534-0
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1997XD79400048"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/S0925-8388(96)02534-0" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Non-Arrhenius conductivity in the fast ionic conductor Li0.05La0.5TiO3:
                        Reconciling spin-lattice and electrical-conductivity relaxations</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Santamaria, J; Paris, MA; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 56 Issue: 9 Pages: 5302-5305 Published:
                        SEP 1 1997<br/><b>Times Cited</b> &nbsp; 73<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.56.5302
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1997XW94200051"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.56.5302" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Electrical conductivity relaxation and nuclear magnetic resonance of Li
                        conducting Li0.5La0.5TiO3</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Lucia, ML; Santamaria, J; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 54 Issue: 1 Pages: 184-189 Published: JUL
                        1 1996<br/><b>Times Cited</b> &nbsp; 74<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.54.184
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1996UW59600039"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.54.184" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Enhancement of persistent photoconductivity by uv excitation in
                        GdBa2CU3O6.3</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Endo, T; Hoffmann, A; Santamaria, J; et al.<br/><b>Source</b>
                        &nbsp; Physical Review B Volume: 54 Issue: 6 Pages: R3750-R3752 Published: AUG 1 1996<br/><b>Times
                            Cited</b> &nbsp; 30
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1996VD67700021"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Influence of epitaxial properties on the mutual inductance response of
                        high-quality YBCO thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Lucia, ML; Santamaria, J; SanchezQuesada,
                        F; et al.<br/><b>Source</b> &nbsp; Physica C Volume: 260 Issue: 1-2 Pages: 149-155 Published:
                        APR 1 1996<br/><b>Times Cited</b> &nbsp; 5<br/><b>DOI</b> &nbsp; 10.1016/0921-4534(96)00120-7
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1996UH47800021"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0921-4534(96)00120-7" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Persistent photoconductivity spectrum of GdBa2Cu3O6.3 in the IR to UV
                        region</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Endo, T; Santamaria, J; Hoffmann, A; et al.<br/><b>Source</b>
                        &nbsp; Czechoslovak Journal of Physics Volume: 46 Pages: 1123-1124 Published: 1996<br/><b>Times
                            Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp; 10.1007/BF02583869
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1996VL43900285"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1007/BF02583869" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Use of Kramers-Kronig transforms for the treatment of admittance
                        spectroscopy data of p-n junctions containing traps</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C; Martin, JM; Santamaria, J; et
                        al.<br/><b>Source</b> &nbsp; Journal of Applied Physics Volume: 79 Issue: 10 Pages: 7830-7836
                        Published: MAY 15 1996<br/><b>Times Cited</b> &nbsp; 10
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1996UK22600056"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>