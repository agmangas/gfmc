<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Collaborations</h1>
                        <hr class="small">
                        <span class="subheading">Physics of Complex Materials Group</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="well">
                    <a class="lead"
                       target="_blank" href="http://www.trt.thalesgroup.com/ump-cnrs-thales/presentation.htm">
                        Unité Mixte de Physique CNRS / Thales
                    </a>
                    <br/>
                    <span class="text-info">
                        Barthelemy A.
                    </span>
                    <span class="text-muted">&</span>
                    <span class="text-info">
                        Bibes M.
                    </span>
                    <span class="text-muted">&</span>
                    <span class="text-info">
                        Villegas J.
                    </span>
                    <br/>
                    <span class="text-muted">
                        <em>
                            France
                        </em>
                    </span>
                </div>
                <div class="well">
                    <a class="lead"
                       target="_blank" href="http://www.icmm.csic.es/emmh/">
                        Group of Electronic and Magnetic Materials and Heterostructures
                    </a>
                    <br/>
                    <span class="text-info">
                        Mar Garcia-Hernandez
                    </span>
                    <br/>
                    <span class="text-muted">
                        <em>
                            Instituto de Ciencia de Materiales de Madrid - Consejo Superior de Investigaciones
                            Científicas (ICMM-CSIC), Spain
                        </em>
                    </span>
                </div>
                <div class="well">
                    <a class="lead"
                       target="_blank" href="http://stem.ornl.gov/index.shtml">
                        The STEM Group
                    </a>
                    <br/>
                    <span class="text-info">
                        Matthew Chisholm
                    </span>
                    <span class="text-muted">&</span>
                    <span class="text-info">
                        Andy Lupini
                    </span>
                    <br/>
                    <span class="text-muted">
                        <em>
                            Materials Science & Technology Division, Oak Ridge National Laboratory, USA
                        </em>
                    </span>
                </div>
                <div class="well">
                    <span class="text-info">
                        Stephen J. Pennycook
                    </span>
                    <br/>
                    <span class="text-muted">
                        <em>
                            University of Tennesee, Knoxville
                        </em>
                    </span>
                </div>
                <div class="well">
                    <span class="text-info">
                        K.L. Ngai
                    </span>
                    <br/>
                    <span class="text-muted">
                        <em>
                            Dipartimento di Fisica, Università di Pisa, Italy
                        </em>
                    </span>
                </div>
                <div class="well">
                    <a class="lead"
                       target="_blank" href="http://www.physics.umn.edu/groups/supercon/">
                        Goldman Superconductivity Research Group
                    </a>
                    <br/>
                    <a class="text-info" target="_blank" href="http://www.physics.umn.edu/people/goldman.html">
                        Allen Goldman
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            University of Minnesota, School of Physics and Astronomy, USA
                        </em>
                    </span>
                </div>
                <div class="well">
                    <a class="lead"
                       target="_blank" href="http://www.cinvestav.edu.mx/saltillo/">
                        CINESTAV
                    </a>
                    <br/>
                    <span class="text-info">
                        A.F. Fuentes
                    </span>
                    <br/>
                    <span class="text-muted">
                        <em>
                            Unidad Saltillo, Mexico
                        </em>
                    </span>
                </div>
                <div class="well">
                    <a class="lead"
                       target="_blank" href="http://www.msd.anl.gov/">
                        Materials Science Division
                    </a>
                    <br/>
                    <a class="text-info" target="_blank" href="http://www.msd.anl.gov/hoffmann">
                        A. Hoffmann
                    </a>
                    <span class="text-muted">&</span>
                    <a class="text-info" target="_blank" href="http://www.msd.anl.gov/te-velthuis">
                        S.G.E. te Velthuis
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            University of Minnesota, School of Physics and Astronomy, USA
                        </em>
                    </span>
                </div>
                <div class="well">
                    <a class="lead" target="_blank" href="http://lansce.lanl.gov/">
                        Los Alamos Neutron Science Centre
                    </a>
                    <span class="text-muted">/</span>
                    <a class="lead" target="_blank" href="http://lansce.lanl.gov/lujan/index.shtml">
                        Lujan Neutron Scattering Centre
                    </a>
                    <br/>
                    <a class="text-info" target="_blank" href="http://lansce.lanl.gov/lujan/instruments/ASTERIX.shtml">
                        Mike Fitzsimmons
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            Los Alamos National Laboratory, USA
                        </em>
                    </span>
                </div>
                <div class="well">
                    <a class="lead" target="_blank"
                       href="http://www.upm.es/observatorio/vi/index.jsp?pageac=grupo.jsp&idGrupo=275">
                        Microsistema y Materiales Electrónicos
                    </a>
                    <span class="text-muted">/</span>
                    <a class="lead" target="_blank" href="http://www.upm.es/observatorio/vi/index.jsp">
                        Observatorio I+D+i
                    </a>
                    <br/>
                    <a class="text-info"
                       target="_blank"
                       href="http://www.upm.es/observatorio/vi/index.jsp?pageac=investigador.jsp&idInvestigador=6328">
                        Enrique Iborra Grau
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            Universidad Politécnica de Madrid, Spain
                        </em>
                    </span>
                </div>
            </div>
        </div>
    </div>

<?php require_once("inc/footer.html") ?>