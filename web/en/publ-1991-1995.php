<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">1991 - 1995</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">A SIMPLE CLOSED-FORM EXPRESSION FOR THE X-RAY REFLECTIVITY FROM MULTILAYERS
                        WITH CUMULATIVE ROUGHNESS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; KELLY, DM; FULLERTON, EE; SANTAMARIA, J; et
                        al.<br/><b>Source</b> &nbsp; Scripta Metallurgica Et Materialia Volume: 33 Issue: 10-11 Pages:
                        1603-1608 Published: DEC 1 1995<br/><b>Times Cited</b> &nbsp; 31<br/><b>DOI</b> &nbsp;
                        10.1016/0956-716X(95)00391-8
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1995TD75800011"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0956-716X(95)00391-8" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">GROWTH OF CHALCOPYRITE CU(IN,GA)SE-2/CUIN3SE5 ABSORBERS BY RADIO-FREQUENCY
                        SPUTTERING</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; HERNANDEZROJAS, JL; MARTIL, I; SANTAMARIA,
                        J; et al.<br/><b>Source</b> &nbsp; Journal of Vacuum Science & Technology a-Vacuum Surfaces and
                        Films Volume: 13 Issue: 3 Pages: 1083-1087 Published: MAY-JUN 1995<br/><b>Times Cited</b> &nbsp;
                        4
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1995RD76600111"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">IONIC-CONDUCTIVITY OF CHEMICALLY LITHIATED YBA2CU3O7 - NMR AND IMPEDANCE
                        SPECTROSCOPIC STUDIES</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; VAREZ, A; LEON, C; SANTAMARIA, J; et
                        al.<br/><b>Source</b> &nbsp; Journal of Physics-Condensed Matter Volume: 7 Issue: 28 Pages:
                        5477-5489 Published: JUL 10 1995<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp;
                        10.1088/0953-8984/7/28/006
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1995RK87300006"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1088/0953-8984/7/28/006" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">EFFECTS OF WEAK INTERGRAIN COUPLING IN THE TRANSPORT-PROPERTIES OF TEXTURED
                        YBCO THIN-FILMS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; LUCIA, ML; SANTAMARIA, J; IBORRA, E; et al.<br/><b>Source</b>
                        &nbsp; Physica C Volume: 225 Issue: 3-4 Pages: 253-261 Published: MAY 20 1994<br/><b>Times
                            Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp; 10.1016/0921-4534(94)90721-8
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1994NP35400007"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0921-4534(94)90721-8" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">STOICHIOMETRY CONTROL OVER A WIDE COMPOSITION RANGE OF SPUTTERED
                        CUGAXIN(1-X)SE2</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; HERNANDEZROJAS, JL; LUCIA, ML; MARTIL, I;
                        et al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 64 Issue: 10 Pages: 1239-1241
                        Published: MAR 7 1994<br/><b>Times Cited</b> &nbsp; 9
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1994MZ17100022"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">A NEW LI-CONDUCTOR BASED ON HTSC PB2SR2Y1-XCAXCU3O8+DELTA</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; SAINTMARD, P; THIBAUT, M; VAREZ, A; et
                        al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 66 Issue: 3-4 Pages: 225-230 Published:
                        NOV 1993<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp; 10.1016/0167-2738(93)90411-U
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1993MM19700005"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0167-2738(93)90411-U" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">A STUDY OF IONIC-CONDUCTIVITY IN DOUBLE RARE-EARTH CHROMATES</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; MELNIKOV, P; BUENO, I; PARADA, C; et
                        al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 63-5 Pages: 581-584 Published: SEP
                        1993<br/><b>Times Cited</b> &nbsp; 5<br/><b>DOI</b> &nbsp; 10.1016/0167-2738(93)90162-V
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1993LZ39400086"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0167-2738(93)90162-V" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">EFFECT OF SPUTTERING PARAMETERS ON THE OXYGENATION DYNAMICS OF 123
                        SUPERCONDUCTING THIN-FILMS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; LUCIA, ML; SANTAMARIA, J; HERNANDEZROJAS,
                        JL; et al.<br/><b>Source</b> &nbsp; Physica C Volume: 204 Issue: 3-4 Pages: 359-364 Published:
                        JAN 1 1993<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp; 10.1016/0921-4534(93)91020-V
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1993KG08900017"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0921-4534(93)91020-V" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">EMISSION-LINE INTENSITIES IN AN RF-SPUTTERING GLOW-DISCHARGE SYSTEM</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; HERNANDEZROJAS, JL; LUCIA, ML; SANTAMARIA,
                        J; et al.<br/><b>Source</b> &nbsp; Thin Solid Films Volume: 228 Issue: 1-2 Pages: 133-136
                        Published: MAY 15 1993<br/><b>Times Cited</b> &nbsp; 3<br/><b>DOI</b> &nbsp;
                        10.1016/0040-6090(93)90581-9
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1993LG37300031"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0040-6090(93)90581-9" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">GRANULARITY EFFECTS IN TRANSPORT-PROPERTIES OF 123 SUPERCONDUCTING
                        THIN-FILMS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; LUCIA, ML; SANTAMARIA, J; HERNANDEZROJAS,
                        JL; et al.<br/><b>Source</b> &nbsp; Journal of Alloys and Compounds Volume: 195 Issue: 1-2
                        Pages: 635-638 Published: MAY 10 1993<br/><b>Times Cited</b> &nbsp; 3<br/><b>DOI</b> &nbsp;
                        10.1016/0925-8388(93)90818-8
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1993LC49200147"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0925-8388(93)90818-8" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">TEXTURE IMPROVEMENT OF SPUTTERED YBA2CU3O7-X FILMS ON MGO (100) WITH A
                        SRTIO3 BUFFER LAYER</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; LUCIA, ML; SANTAMARIA, J; IBORRA, E; et al.<br/><b>Source</b>
                        &nbsp; Physica C Volume: 218 Issue: 1-2 Pages: 59-62 Published: DEC 1 1993<br/><b>Times
                            Cited</b> &nbsp; 9<br/><b>DOI</b> &nbsp; 10.1016/0921-4534(93)90265-R
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1993MM29800010"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0921-4534(93)90265-R" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">CHALCOPYRITE CUGAXIN1-XSE2 SEMICONDUCTING THIN-FILMS PRODUCED BY
                        RADIO-FREQUENCY SPUTTERING</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; HERNANDEZROJAS, JL; LUCIA, ML; MARTIL, I;
                        et al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 60 Issue: 15 Pages: 1875-1877
                        Published: APR 13 1992<br/><b>Times Cited</b> &nbsp; 10
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1992HN48100035"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">OPTICAL ANALYSIS OF ABSORBING THIN-FILMS - APPLICATION TO TERNARY
                        CHALCOPYRITE SEMICONDUCTORS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; HERNANDEZROJAS, JL; LUCIA, ML; MARTIL, I;
                        et al.<br/><b>Source</b> &nbsp; Applied Optics Volume: 31 Issue: 10 Pages: 1606-1611 Published:
                        APR 1 1992<br/><b>Times Cited</b> &nbsp; 33
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1992HM11000031"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">OPTICAL SPECTROSCOPIC STUDY OF THE GROWTH DYNAMICS OF
                        RADIO-FREQUENCY-SPUTTERED YBA2CU3O7-X THIN-FILMS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; LUCIA, ML; HERNANDEZROJAS, JL; SANTAMARIA,
                        J; et al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 61 Issue: 2 Pages: 231-233
                        Published: JUL 13 1992<br/><b>Times Cited</b> &nbsp; 11
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1992JD35200038"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Influence of production conditions on physical properties of  Y-Ba-Cu-O
                        thin films produced by RF sputtering</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Lucia, M.L.; Fernandez-Rojas, J.L.; Martil,
                        I.; et al.<br/><b>Source</b> &nbsp; Anales de Fisica, Serie B (Aplicaciones, Metodos e
                        Instrumentos) Volume: 87 Issue: 1 Pages: 18-28 Published: Jan.-Dec. 1991
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=INSPEC&amp;KeyUT=4298251"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">SYNTHESIS AND IONIC-CONDUCTIVITY OF RB(MOSB)O6</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; GARCIAMARTIN, S; VEIGA, ML; JEREZ, A; et
                        al.<br/><b>Source</b> &nbsp; European Journal of Solid State and Inorganic Chemistry Volume: 28
                        Issue: 2 Pages: 363-371 Published: 1991<br/><b>Times Cited</b> &nbsp; 2
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1991FG67200002"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>