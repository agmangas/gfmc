<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">2008</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">Cation size effects in oxygen ion dynamics of highly disordered
                        pyrochlore-type ionic conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Diaz-Guillen, M. R.; Moreno, K. J.;
                        Diaz-Guillen, J. A.; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 78 Issue: 10
                        Published: SEP 2008<br/><b>Times Cited</b> &nbsp; 20<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevB.78.104304
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000259690400046"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.78.104304" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2008/DiazGuillenetalPhysRevB782008p104304.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Colossal ionic conductivity at interfaces of epitaxial ZrO(2):
                        Y(2)O(3)/SrTiO(3) heterostructures</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Garcia-Barriocanal, J.; Rivera-Calzada, A.;
                        Varela, M.; et al.<br/><b>Source</b> &nbsp; Science Volume: 321 Issue: 5889 Pages: 676-680
                        Published: AUG 1 2008<br/><b>Times Cited</b> &nbsp; 180<br/><b>DOI</b> &nbsp;
                        10.1126/science.1156393
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000258077700040"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1126/science.1156393" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2008/GarciaBarriocanaletalScience3212008p676.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Effects of interface states on the transport properties of all-oxide
                        La(0.8)Sr(0.2)CoO(3)/SrTi(0.99)Nb(0.01)O(3) p-n heterojunctions</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Bruno, F. Y.; Garcia-Barriocanal, J.;
                        Torija, M.; et al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 92 Issue: 8
                        Published: FEB 25 2008<br/><b>Times Cited</b> &nbsp; 14<br/><b>DOI</b> &nbsp; 10.1063/1.2887905
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000254297300044"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.2887905" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2008/BrunoetalApplPhysLett922008p082106.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Elucidating the existence of the excess wing in an ionic liquid on applying
                        pressure</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera-Calzada, A.; Kaminski, K.; Leon, C.;
                        et al.<br/><b>Source</b> &nbsp; Journal of Physics-Condensed Matter Volume: 20 Issue: 24
                        Published: JUN 18 2008<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp;
                        10.1088/0953-8984/20/24/244107
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000256556400008"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1088/0953-8984/20/24/244107" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2008/RiveraCalzadaetalJPhysCondensMatter202008p244107.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Ferromagnetic resonance of ultrathin Co/Ag superlattices on Si(111)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Kakazei, G. N.; Martin, P. P.; Ruiz, A.; et
                        al.<br/><b>Source</b> &nbsp; Journal of Applied Physics Volume: 103 Issue: 7 Published: APR 1
                        2008<br/><b>Times Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp; 10.1063/1.2839287
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000255043200261"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.2839287" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2008/KakazeietalJApplPhys1032008p07B527.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">High ionic conductivity in the pyrochlore-type Gd(2-y)La(y)Zr(2)O(7) solid
                        solution (0 <= y <= 1)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Diaz-Guillen, J. A.; Diaz-Guillen, M. R.;
                        Padmasree, K. P.; et al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 179 Issue: 38
                        Pages: 2160-2164 Published: NOV 30 2008<br/><b>Times Cited</b> &nbsp; 36<br/><b>DOI</b> &nbsp;
                        10.1016/j.ssi.2008.07.015
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000262201600006"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.ssi.2008.07.015" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2008/DiazGuillenetalSolidStateIonics1792008p2160.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Ion dynamics under pressure in an ionic liquid</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Rivera-Calzada, A.; Kaminski, K.; Leon, C.;
                        et al.<br/><b>Source</b> &nbsp; Journal of Physical Chemistry B Volume: 112 Issue: 10 Pages:
                        3110-3114 Published: MAR 13 2008<br/><b>Times Cited</b> &nbsp; 24<br/><b>DOI</b> &nbsp;
                        10.1021/jp710479b
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000253784700045"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1021/jp710479b" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2008/RiveraCalzadaetalJPhysChemB1122008p3110.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Origin of the inverse spin-switch behavior in manganite/cuprate/manganite
                        trilayers</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Nemes, N. M.; Garcia-Hernandez, M.;
                        Velthuis, S. G. E. te; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 78 Issue: 9
                        Published: SEP 2008<br/><b>Times Cited</b> &nbsp; 29<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevB.78.094515
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000259689700091"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.78.094515" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2008/NemesetalPhysRevB782008p094515.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Stray field and spin-imbalance La0.7Ca0.3MnO3/YBa2Cu3O7-delta effects in
                        multilayers</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Hu, T.; Xao, H.; Visani, C.; et al.<br/><b>Source</b>
                        &nbsp; Physica B-Condensed Matter Volume: 403 Issue: 5-9 Pages: 1167-1169 Published: APR 1
                        2008<br/><b>Times Cited</b> &nbsp; 7<br/><b>DOI</b> &nbsp; 10.1016/j.physb.2007.10.221
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000254689900163"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.physb.2007.10.221" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2008/HuetalPhysicaB4032008p1167.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Synthesis and electrical properties of the pyrochlore-type
                        Gd(2-y)La(y)Zr(2)O(7) solid solution</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Diaz-Guillen, J. A.; Diaz-Guillen, A. R.;
                        Padmasree, K. P.; et al.<br/><b>Source</b> &nbsp; Boletin De La Sociedad Espanola De Ceramica Y
                        Vidrio Volume: 47 Issue: 3 Pages: 159-164 Published: MAY-JUN 2008<br/><b>Times Cited</b> &nbsp;
                        0
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000261971400009"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="../../files/publications/2008/DiazGuillenetalBolSocEspCeramVidrio472008p159.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Thickness Dependent Magnetic Anisotropy of Ultrathin LCMO Epitaxial Thin
                        Films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Nemes, Norbert Marcel; Garcia-Hernandez,
                        Mar; Szatmari, Zsolt; et al.<br/><b>Source</b> &nbsp; Ieee Transactions on Magnetics Volume: 44
                        Issue: 11 Pages: 2926-2929 Published: NOV 2008<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b>
                        &nbsp; 10.1109/TMAG.2008.2001523
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000262221200123"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1109/TMAG.2008.2001523" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2008/NemesetalIEEETransMagn442008p2926.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">The Gd La Zr O  solid solution as a new electrolyte for high and
                        intermediate-temperature SOFC's</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Diaz-Guillen, M.R.; Diaz-Guillen, J.A.;
                        Fuentes, A.F.; et al.<br/><b>Source</b> &nbsp; Fuel Cell Seminar 2007 Pages: 333-42 Published:
                        2008<br/><b>DOI</b> &nbsp; 10.1149/1.2921559
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=INSPEC&amp;KeyUT=11111204"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1149/1.2921559" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>