<?php require_once "inc/header.html";?>
<?php require_once "inc/navigation.html";?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <div class="page-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Physics of Complex Materials Group</h1>
                            <hr class="small">
                            <span class="subheading">
                                Complutense University of Madrid
                            </span>
                            <p style="margin-bottom: 0;">
                                ICMM-CSIC Associate Unit: Laboratory of heterostructures for Spintronics applications
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="text-center">
                <img src="../../img/group-photo.jpg" class="img-thumbnail">
            </div>

            <h2 class="add-margin-bottom add-margin-top">Major research lines</h2>

            <div class="row" id="major-research-lines-row">
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            Quantum matter. Correlated Oxides. nanoestructures and heterostructures, interface phenomena. Magnetism. Superconductivity.
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            Ion transport, oxygen vacancies. Dielectric Spectroscopy with emphasis on the study of magnetoelectric coupling.
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            Spintronics. Magnetic Tunnel Junctions based on Transition Metal Oxides. Domain Wall engineering: nanowires.
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="add-margin-bottom">Recent news and updates</h2>

            <div class="panel-group add-margin-bottom accordion-news" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#news-08">
                                <span class="text-muted">
                                    <b>Apr 09, 2018</b>
                                </span>
                                &nbsp; New Book
                            </a>
                        </h4>
                    </div>
                    <div id="news-08" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p>
                                <a href="http://www.springer.com/us/book/9783319423890" target="_blank">
                                    <i class="fa fa-fw fa-external-link"></i>
                                    View on springer.com
                                </a>
                            </p>

                            <p>
                                <i>Dynamics of Glassy, Crystalline and Liquid Ionic Conductors: Experiments, Theories, Simulations</i>
                                published by Springer-Nature in 2016.
                            </p>

                            <p class="text-muted">
                                Authors: Junko Habasaki, Carlos León & Kia Ngai.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#news-07">
                                <span class="text-muted">
                                    <b>Apr 09, 2018</b>
                                </span>
                                &nbsp; Modified magnetic anisotropy at LaCoO3/La0.7Sr0.3MnO3 interfaces
                            </a>
                        </h4>
                    </div>
                    <div id="news-07" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p>
                                <a href="https://aip.scitation.org/doi/full/10.1063/1.5002090" target="_blank">
                                    <i class="fa fa-fw fa-external-link"></i>
                                    View on aip.scitation.org
                                </a>
                            </p>

                            <p class="text-muted">
                                M. Cabero, K. Nagy, F. Gallego, A. Sander, M. Rio, F.A. Cuellar, J.Tornos, D. Hernandez-Martin, N.M. Nemes, F. Mompean, M.
                                Garcia-Hernandez, A. Rivera-Calzada, Z. Sefrioui, N. Reyren, T. Feher, M. Varela, C. Leon
                                y J. Santamaria. APL Materials 5, 096104 (2017). doi: 10.1063/1.5002090
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#news-06">
                                <span class="text-muted">
                                    <b>Apr 09, 2018</b>
                                </span>
                                &nbsp; Resonant electron tunnelling assisted by charged domain walls in multiferroic tunnel junctions
                            </a>
                        </h4>
                    </div>
                    <div id="news-06" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p>
                                <a href="https://www.nature.com/articles/nnano.2017.51" target="_blank">
                                    <i class="fa fa-fw fa-external-link"></i>
                                    View on nature.com
                                </a>
                            </p>

                            <p class="text-muted">
                                G. Sanchez-Santolino, J. Tornos, D. Hernandez-Martin, J. I. Beltran,C. Munuera, M. Cabero, A. Perez-Muñoz, J. Ricote, F.
                                Mompean, M. Garcia-Hernandez, Z. Sefrioui, C. Leon, S. J.Pennycook, M. C. Muñoz, M. Varela,
                                J. Santamaria Nat Nano 12, 655 (2017). doi: 10.1038/nnano.2017.51
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#news-05">
                                <span class="text-muted">
                                    <b>Feb 02, 2017</b>
                                </span> &nbsp; New publication in PNAS
                            </a>
                        </h4>
                    </div>
                    <div id="news-05" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p class="lead">
                                <a href="http://www.pnas.org/content/114/2/215.abstract" target="_blank">
                                    In operando evidence of deoxygenation in ionic liquid gating of YBa2Cu3O7-X
                                </a>
                            </p>

                            <p class="text-muted">
                                Ana M. Perez-Muñoz, Pedro Schio, Roberta Poloni, Alejandro Fernandez-Martinez, Alberto Rivera-Calzada, Julio C. Cezar, Eduardo
                                Salas-Colera, German R. Castro, Joseph Kinney, Carlos Leon, Jacobo Santamaria, Javier Garcia-Barriocanal,
                                and Allen M. Goldman
                            </p>

                            <p>
                                <a href="../../files/misc/nota-superconductividad-oxidos-complejos.pdf">
                                    <i class="fa fa-fw fa-file-pdf-o"></i>
                                    Note
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="add-margin-bottom add-margin-top-big">Research highlights</h2>

            <p class="text-justify">
                The group started in 1998. In the last 10 years we have put a continued effort on the physics of complex oxides interfaces.
                Materials include colossal magnetoresistance manganites, high Tc superconducting cuprates, Mott and band
                insulators, and solid electrolytes. Structures and devices are superlattices, magnetic tunnel junctions,
                and planar nanostructures for lateral transport fabricated by optical and e-beam lithography.
            </p>

            <div class="row">
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small">
                        <small>
                            RESONANT ELECTRON TUNNELLING ASSISTED BY CHARGED DOMAIN WALLS IN MULTIFERROIC TUNNEL JUNCTIONS
                        </small>
                    </p>
                    <a href="https://www.nature.com/articles/nnano.2017.51">
                        <img src="../../img/research-highlight-nat-nanotech.jpg" class="img-thumbnail">
                    </a>
                    <span class="caption text-muted">
                        Nat Nano 12, 655 (2017). doi: 10.1038/nnano.2017.51.
                    </span>
                </div>
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small">
                        <small>
                            IN OPERANDO EVIDENCE OF DEOXYGENATION IN IONIC LIQUID GATING OF YBa2Cu3O7-x
                        </small>
                    </p>
                    <a href="http://www.pnas.org/content/114/2/215">
                        <img src="../../img/research-highlight-pnas.jpg" class="img-thumbnail">
                    </a>
                    <span class="caption text-muted">
                        Proc. Natl. Acad. Sci. USA 114, 215 (2017). doi: 10.1073/pnas.1613006114.
                    </span>
                </div>
            </div>

            <div class="row add-margin-top">
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small">
                        <small>
                            INSIGHT INTO SPIN TRANSPORT IN OXIDE HETEROSTRUCTURES FROM INTERFACE-RESOLVED MAGNETIC MAPPING
                        </small>
                    </p>
                    <img src="../../img/research-highlights-natcom2014-2.jpg" class="img-thumbnail">
                    <span class="caption text-muted">
                        Nat. Commun. 6:6306 DOI: 10.1038/ncomms7306 (2015)
                    </span>
                </div>
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small">
                        <small>
                            COMPETITION BETWEEN COVALENT BONDING AND CHARGE TRANSFER AT COMPLEX-OXIDE INTERFACES
                        </small>
                    </p>
                    <img src="../../img/research-highlights-physrev2014.jpg" class="img-thumbnail">
                    <span class="caption text-muted">
                        Physical Review Letters 112, 196802 DOI: 10.1103/PhysRevLett.112.196802 (2014)
                    </span>
                </div>
            </div>

            <div class="row add-margin-top">
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small">
                        <small>REVERSIBLE ELECTRIC-FIELD CONTROL OF MAGNETIZATION AT OXIDE INTERFACES</small>
                    </p>
                    <img src="../../img/research-highlights-natcom2014.jpg" class="img-thumbnail">
                    <span class="caption text-muted">Nature Commun. 5:4215 DOI: 10.1038/ncomms5215 (2014)</span>
                </div>
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small">
                        <small>
                            ELECTRON DOPING BY CHARGE TRANSFER AT LAFEO
                            <sub>3</sub>/SM
                            <sub>2</sub>CUO
                            <sub>4</sub>
                            EPITAXIAL INTERFACES
                        </small>
                    </p>
                    <img src="../../img/research-highlights-advmat2013.jpg" class="img-thumbnail">
                    <span class="caption text-muted">Adv. Mat. 25, 1468 (2013)</span>
                </div>
            </div>

            <div class="row add-margin-top">
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small">
                        <small>EQUAL-SPIN ANDREEV REFLECTION AND LONG RANGE COHERENT TRANSPORT IN HIGH-TEMPERATURE SUPERCONDUCTOR/HALF-METALLIC
                            FERROMAGNET JUNCTIONS
                        </small>
                    </p>
                    <img src="../../img/research-highlights-natphys2012.jpg" class="img-thumbnail">
                    <span class="caption text-muted">Nature Physics 8, 539 DOI:10.1038/nphys2318 (2012)</span>
                </div>
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small" style="padding-left: 30px; padding-right: 30px;">
                        <small>TAILORING INTERFACE STRUCTURE IN HIGHLY STRAINED YSZ/STO HETEROSTRUCTURES</small>
                    </p>
                    <img src="../../img/research-highlights-advmat2011.jpg" class="img-thumbnail">
                    <span class="caption text-muted">Adv. Mat. 23, 5268 (2011)</span>
                </div>
            </div>

            <div class="row add-margin-top">
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small">
                        <small>INTERFACE RECONSTRUCTION AT LCMO/YBCO/LCMO HETEROSTRUCTURES</small>
                    </p>
                    <img src="../../img/research-highlights-04.jpg" class="img-thumbnail">
                    <span class="caption text-muted">Phys. Rev. B 84, 060405 (R) (2011)</span>
                </div>
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small">
                        <small>LOW DIMENSIONAL MAGNETISM DUE TO SPIN AND ORBITAL ELECTRONIC RECONSTRUCTION IN LAMNO
                            <sub>3</sub>/SRTIO
                            <sub>3</sub> INTERFACES
                        </small>
                    </p>
                    <img src="../../img/research-highlights-01.jpg" class="img-thumbnail">
                    <span class="caption text-muted">Nature Comm. 1: 82, (2010)</span>
                </div>
            </div>

            <div class="row add-margin-top">
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small" style="padding-left: 30px; padding-right: 30px;">
                        <small>SPIN TRANSPORT IN ALL-MANGANITE MAGNETIC TUNNEL JUNCTIONS</small>
                    </p>
                    <img src="../../img/research-highlights-03.jpg" class="img-thumbnail">
                    <span class="caption text-muted">Advanced Materials 22, 5029 (2010)</span>
                </div>
                <div class="col-md-6 text-center">
                    <p class="add-margin-bottom-small">
                        <small>ENHANCED IONIC CONDUCTIVITY AT YSZ/STO INTERFACES</small>
                    </p>
                    <img src="../../img/research-highlights-02.jpg" class="img-thumbnail">
                    <span class="caption text-muted">Science 321, 676 (2008)</span>
                </div>
            </div>
        </div>

    </div>
</div>
</div>

<?php require_once "inc/footer.html"?>