<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Financial Support</h1>
                        <hr class="small">
                        <span class="subheading">Physics of Complex Materials Group</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="well">
                    <a class="lead"
                       target="_blank" href="http://cordis.europa.eu/project/rcn/92717_en.html">
                        ERC Starting Grant - STEMOX: Under the light of electrons
                    </a><br/>
                    <span class="text-muted">
                        <em>
                            European Union
                        </em>
                    </span>
                </div>
                <div class="well">
                    <a class="lead"
                       target="_blank"
                       href="http://www.imagine-csd2009.es/index.php?option=com_content&view=article&id=1:consolideringenio&catid=25:the-project">
                        Consolider IMAGINE
                    </a><br/>
                    <span class="text-muted">
                        <em>
                            Ministry of Economy, Spain
                        </em>
                    </span>
                </div>
                <div class="well">
                    <a class="lead"
                       target="_blank" href="http://phama.icmm.csic.es/?q=node/1">
                        PHAMA: Hybrid Advanced Materials for Photonic Applications
                    </a><br/>
                    <span class="text-muted">
                        <em>
                            Community of Madrid (regional Ministry of Education), European Union (European Social Fund)
                        </em>
                    </span>
                </div>
                <div class="well">
                    <span class="lead">
                        OXIRIS: Complex Oxide Interfaces In Spintronics: Understanding, Tailoring And Applying
                    </span><br/>
                    <span class="text-muted">
                        <em>
                            Ministry of Science and Innovation (MICINN), Spain
                        </em>
                    </span>
                </div>
            </div>
        </div>
    </div>

<?php require_once("inc/footer.html") ?>