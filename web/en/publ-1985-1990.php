<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">1985 - 1990</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">BARRIER EFFECTS ON IONIC-CONDUCTIVITY AND DIELECTRIC RESPONSE OF
                        TL(NBTE)O6</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; GARCIAMARTIN, S; VEIGA, ML; PICO, C; et al.<br/><b>Source</b>
                        &nbsp; Solid State Ionics Volume: 44 Issue: 1-2 Pages: 131-137 Published: DEC 1990<br/><b>Times
                            Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp; 10.1016/0167-2738(90)90054-U
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1990ET30200019"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0167-2738(90)90054-U" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">DIELECTRIC RESPONSE AND IONIC-CONDUCTIVITY OF CS(NBTE)O6</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; GARCIAMARTIN, S; VEIGA, ML; PICO, C; et al.<br/><b>Source</b>
                        &nbsp; Materials Research Bulletin Volume: 25 Issue: 11 Pages: 1393-1399 Published: NOV
                        1990<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp; 10.1016/0025-5408(90)90222-N
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1990EK87700008"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0025-5408(90)90222-N" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">ELECTRICAL CHARACTERIZATION OF ALL-SPUTTERED CDS/CUINSE2 SOLAR-CELL
                        HETEROJUNCTIONS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; SANTAMARIA, J; MARTIL, I; IBORRA, E; et al.<br/><b>Source</b>
                        &nbsp; Solar Cells Volume: 28 Issue: 1 Pages: 31-39 Published: JAN 1990<br/><b>Times Cited</b>
                        &nbsp; 10<br/><b>DOI</b> &nbsp; 10.1016/0379-6787(90)90036-5
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1990CM44600004"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0379-6787(90)90036-5" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">GROWTH AND PHYSICAL-PROPERTIES OF CUGASE2 THIN-FILMS BY RF-SPUTTERING</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; MARTIL, I; GONZALEZDIAZ, G; SANTAMARIA, J;
                        et al.<br/><b>Source</b> &nbsp; Journal of Materials Science Letters Volume: 9 Issue: 2 Pages:
                        237-240 Published: FEB 1990<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp;
                        10.1007/BF00727730
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1990CQ29000040"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1007/BF00727730" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">IONIC-CONDUCTIVITY OF LITHIUM INSERTED BA2YCU3O7-Y</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; VAREZ, A; MORAN, E; ALARIOFRANCO, MA; et
                        al.<br/><b>Source</b> &nbsp; Solid State Communications Volume: 76 Issue: 7 Pages: 917-920
                        Published: NOV 1990<br/><b>Times Cited</b> &nbsp; 17<br/><b>DOI</b> &nbsp;
                        10.1016/0038-1098(90)90883-D
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1990EJ56300010"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0038-1098(90)90883-D" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">LITHIUM INSERTION IN BA2YCU3O7-Y</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; ALARIOFRANCO, MA; MORAN, E; VAREZ, A; et
                        al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 44 Issue: 1-2 Pages: 73-80 Published:
                        DEC 1990<br/><b>Times Cited</b> &nbsp; 20<br/><b>DOI</b> &nbsp; 10.1016/0167-2738(90)90047-U
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1990ET30200012"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0167-2738(90)90047-U" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">STRUCTURAL, ELECTRICAL, AND OPTICAL-PROPERTIES OF CUGASE2 RF-SPUTTERED
                        THIN-FILMS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; MARTIL, I; SANTAMARIA, J; DIAZ, GG; et
                        al.<br/><b>Source</b> &nbsp; Journal of Applied Physics Volume: 68 Issue: 1 Pages: 189-194
                        Published: JUL 1 1990<br/><b>Times Cited</b> &nbsp; 19
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1990DK37700032"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">ROLE OF DEEP LEVELS AND INTERFACE STATES IN THE CAPACITANCE CHARACTERISTICS
                        OF ALL-SPUTTERED CULNSE2/CDS SOLAR-CELL HETEROJUNCTIONS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; SANTAMARIA, J; DIAZ, GG; IBORRA, E; et
                        al.<br/><b>Source</b> &nbsp; Journal of Applied Physics Volume: 65 Issue: 8 Pages: 3236-3241
                        Published: APR 15 1989<br/><b>Times Cited</b> &nbsp; 6<br/><b>DOI</b> &nbsp; 10.1063/1.342676
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1989T892200052"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.342676" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">SUBSTRATE-TEMPERATURE EFFECT ON THE OPTICAL-PROPERTIES OF RADIO-FREQUENCY
                        SPUTTERED CUINSE2 THIN-FILMS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; SANTAMARIA, J; MARTIL, I; IBORRA, E; et al.<br/><b>Source</b>
                        &nbsp; Journal of Vacuum Science & Technology a-Vacuum Surfaces and Films Volume: 7 Issue: 3
                        Pages: 1424-1427 Published: MAY-JUN 1989<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp;
                        10.1116/1.576296
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1989U715300160"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1116/1.576296" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">SYNTHESIS, CHARACTERIZATION AND IONIC-CONDUCTIVITY OF TL(NBTE)O6</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; GARCIAMARTIN, S; VEIGA, ML; JEREZ, A; et
                        al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 37 Issue: 1 Pages: 87-93 Published: DEC
                        1989<br/><b>Times Cited</b> &nbsp; 5<br/><b>DOI</b> &nbsp; 10.1016/0167-2738(89)90293-2
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1989CC58600014"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0167-2738(89)90293-2" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">CAPACITANCE CHARACTERIZATION OF CU2S/CDS HETEROJUNCTIONS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; SANTAMARIA, J; IBORRA, E; MARTIL, I; et al.<br/><b>Source</b>
                        &nbsp; Semiconductor Science and Technology Volume: 3 Issue: 8 Pages: 781-785 Published: AUG
                        1988<br/><b>Times Cited</b> &nbsp; 9<br/><b>DOI</b> &nbsp; 10.1088/0268-1242/3/8/008
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1988P541300008"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1088/0268-1242/3/8/008" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">EFFECTS OF ARGON PARTIAL-PRESSURE AND HYDROGEN ADMIXTURES ON THE PROPERTIES
                        OF SPUTTERED CUINSE2 THIN-FILMS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; SANCHEZQUESADA, F; CASE, C; GONZALEZDIAZ,
                        G; et al.<br/><b>Source</b> &nbsp; Applied Surface Science Volume: 33-4 Pages: 844-853
                        Published: SEP 1988<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp;
                        10.1016/0169-4332(88)90389-3
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1988Q431500110"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0169-4332(88)90389-3" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">INFLUENCE OF INTERFACE STATES ON THE ELECTRICAL CHARACTERISTICS OF
                        ALL-SPUTTERED CUXS/CDS SOLAR-CELLS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; IBORRA, E; SANTAMARIA, J; MARTIL, I; et al.<br/><b>Source</b>
                        &nbsp; Solar Energy Materials Volume: 17 Issue: 4 Pages: 279-287 Published: JUN 1988<br/><b>Times
                            Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp; 10.1016/0165-1633(88)90056-1
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1988N824900005"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0165-1633(88)90056-1" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">IONIC-CONDUCTIVITY OF MIXED K(NBW)O6 AND KH2O(NBW)O6</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; GARCIAMARTIN, S; VEIGA, ML; GAITAN, M; et
                        al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 27 Issue: 3 Pages: 195-198 Published:
                        JUL 1988<br/><b>Times Cited</b> &nbsp; 6<br/><b>DOI</b> &nbsp; 10.1016/0167-2738(88)90010-0
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1988Q110600010"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0167-2738(88)90010-0" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">CUINSE2 THIN-FILMS PRODUCED BY RF-SPUTTERING IN AR/H2 ATMOSPHERES</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; MARTIL, I; SANTAMARIA, J; IBORRA, E; et al.<br/><b>Source</b>
                        &nbsp; Journal of Applied Physics Volume: 62 Issue: 10 Pages: 4163-4169 Published: NOV 15
                        1987<br/><b>Times Cited</b> &nbsp; 32<br/><b>DOI</b> &nbsp; 10.1063/1.339135
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1987K700700025"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.339135" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">EFFECTS OF RESIDUAL GASES AND RF POWER ON ITO RF-SPUTTERED THIN-FILMS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; CLEMENT, M; SANTAMARIA, J; IBORRA, E; et
                        al.<br/><b>Source</b> &nbsp; Vacuum Volume: 37 Issue: 5-6 Pages: 447-449 Published: 1987<br/><b>Times
                            Cited</b> &nbsp; 7<br/><b>DOI</b> &nbsp; 10.1016/0042-207X(87)90333-2
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1987K759400018"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0042-207X(87)90333-2" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">RF sputtered CuInSe  thin films in Ar/H   atmospheres</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Santamaria, J.; Iborra, E.; Martil, I.; et
                        al.<br/><b>Source</b> &nbsp; Seventh E.C. Photovoltaic Solar Energy Conference. Proceedings of
                        the International Conference (EUR-10939-EN) Pages: 1223-5 Published: 1987
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=INSPEC&amp;KeyUT=3096516"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">SPUTTERING PROCESS OF CU2S IN AN AR ATMOSPHERE</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; SANTAMARIA, J; IBORRA, E; MARTIL, I; et al.<br/><b>Source</b>
                        &nbsp; Vacuum Volume: 37 Issue: 5-6 Pages: 433-436 Published: 1987<br/><b>Times Cited</b> &nbsp;
                        2<br/><b>DOI</b> &nbsp; 10.1016/0042-207X(87)90328-9
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1987K759400013"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0042-207X(87)90328-9" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">THIN CUXS SPUTTERED FILMS IN AR/H2 ATMOSPHERES</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; IBORRA, E; SANTAMARIA, J; MARTIL, I; et al.<br/><b>Source</b>
                        &nbsp; Vacuum Volume: 37 Issue: 5-6 Pages: 437-439 Published: 1987<br/><b>Times Cited</b> &nbsp;
                        7<br/><b>DOI</b> &nbsp; 10.1016/0042-207X(87)90329-0
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1987K759400014"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0042-207X(87)90329-0" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Effects of growth temperature on composition of thin copper sulfide
                         films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Martil, I.; Santamaria, J.; Iborra, E.; et
                        al.<br/><b>Source</b> &nbsp; Anales de Fisica, Serie B (Aplicaciones, Metodos e Instrumentos)
                        Volume: 82 Issue: 3 Pages: 301-4 Published: Aug.-Dec. 1986
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=INSPEC&amp;KeyUT=2910587"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">SPUTTERING OF SIO2 IN O2-AR ATMOSPHERES</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; SANTAMARIA, J; IBORRA, E; QUESADA, FS; et
                        al.<br/><b>Source</b> &nbsp; Thin Solid Films Volume: 139 Issue: 2 Pages: 201-208 Published: MAY
                        15 1986<br/><b>Times Cited</b> &nbsp; 9<br/><b>DOI</b> &nbsp; 10.1016/0040-6090(86)90338-X
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1986C866700011"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0040-6090(86)90338-X" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">ELECTRICAL-PROPERTIES OF RF-SPUTTERED SIO2-FILMS</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; SANTAMARIA, J; QUESADA, FS; DIAZ, GG; et
                        al.<br/><b>Source</b> &nbsp; Thin Solid Films Volume: 125 Issue: 3-4 Pages: 299-303 Published:
                        1985<br/><b>Times Cited</b> &nbsp; 3<br/><b>DOI</b> &nbsp; 10.1016/0040-6090(85)90236-6
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=A1985AQD5700014"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/0040-6090(85)90236-6" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Influence of hydrogen on photovoltaic thin Cu S  sputtered films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Iborra, E.; Santamaria, J.; Martil, I.; et
                        al.<br/><b>Source</b> &nbsp; Sixth E.C. Photovoltaic Solar Energy Conference Pages: 871-3
                        Published: 1985
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=INSPEC&amp;KeyUT=2678482"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>