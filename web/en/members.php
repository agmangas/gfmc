<?php require_once "inc/header.html";?>
<?php require_once "inc/navigation.html";?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>Members</h1>
                    <hr class="small">
                    <span class="subheading">Physics of Complex Materials Group</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container members">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <h2 class="add-margin-bottom">Professors</h2>

            <!-- Jacobo Santamaria -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp;
                    <span class="text-muted">
                        <b>HEAD</b>
                    </span> &nbsp; Prof. Jacobo Santamaría Sánchez-Barriga
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-jacobo-santamaria.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>jacsan@fis.ucm.es</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>0034 91 394 4435</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Office</th>
                                        <td>118.0 (3rd floor)</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Links</th>
                                        <td>
                                            <a href="../../files/cvs/cva-jacobo-santamaria.pdf" target="_blank">
                                                Curriculum Vitae
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Carlos Leon -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Prof. Carlos León Yebra
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-carlos-leon.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>carlos.leon@fis.ucm.es</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>0034 91 394 5212</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Office</th>
                                        <td>122.0 (3rd floor)</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Links</th>
                                        <td>
                                            <a target="_blank" href="http://www.researcherid.com/rid/A-5587-2008">
                                                ResearcherID
                                            </a> &nbsp;
                                            <a href="../../files/cvs/cva-carlos-leon.pdf" target="_blank">
                                                Curriculum Vitae
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Norbert Nemes -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Prof. Norbert Nemes
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-norbert-nemes.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>nmnemes@fis.ucm.es</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>0034 91 394 4320</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Office</th>
                                        <td>121</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Alberto Rivera -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Prof. Alberto Rivera Calzada
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-alberto-rivera.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>alberto.rivera@fis.ucm.es</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>0034 91 394 4388</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Office</th>
                                        <td>120</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Links</th>
                                        <td>
                                            <a href="../../files/cvs/cva-alberto-rivera-2.pdf" target="_blank">
                                                Curriculum Vitae
                                            </a>

                                            &nbsp;

                                            <a href="http://www.researcherid.com/rid/C-4802-2013" target="_blank">
                                                ResearcherID
                                            </a>

                                            &nbsp;

                                            <a href="http://orcid.org/0000-0001-6744-590X" target="_blank">
                                                ORCID
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Rainer Schmidt -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Prof. Rainer Schmidt
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-rainer-schmidt.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>rainer.schmidt@fis.ucm.es</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>0034 91 394 4320</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Fax</th>
                                        <td>91394 4547</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Office</th>
                                        <td>121</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Links</th>
                                        <td>
                                            <a href="http://www.researchgate.net/profile/Rainer_Schmidt6" target="_blank">
                                                ResearchGate
                                            </a>
                                            &nbsp;
                                            <a href="http://www.researcherid.com/rid/A-4265-2008" target="_blank">
                                                ResearcherID
                                            </a>
                                            &nbsp;
                                            <a href="../../files/cvs/cva-rainer-schmidt.pdf" target="_blank">
                                                Curriculum Vitae
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">ORCID</th>
                                        <td>0000-0002-8344-8403</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Zouhair Sefrioui -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Prof. Zouhair Sefrioui
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-zouhair-sefrioui.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>sefrioui@fis.ucm.es</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>0034 91 394 4151</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Office</th>
                                        <td>116</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Links</th>
                                        <td>
                                            <a href="../../files/cvs/cva-zouhair-sefrioui.pdf" target="_blank">
                                                Curriculum Vitae
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- María Varela del Arco -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Prof. María Varela del Arco
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-maria-varela.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>mvarela@ucm.es</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>0034 91 394 4395</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Office</th>
                                        <td>117</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="add-margin-bottom add-margin-top-big">Postdoctoral Researcher</h2>

            <!-- Javier Tornos -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Dr. Javier Tornos
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-javier-tornos.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>91394 5220</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>jtornos@fis.ucm.es</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Fabian Cuellar -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Dr. Fabian Andres Cuellar Jimenez
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-fabian-cuellar.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>91394 5220</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>f.cuellar@fis.ucm.es</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Links</th>
                                        <td>
                                            <a href="https://orcid.org/0000-0002-2891-6198" target="_blank">ORCID</a> &nbsp;
                                            <a href="https://www.scopus.com/authid/detail.uri?authorId=14324735500" target="_blank">Scopus</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Neven Biskup -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Dr. Neven Biskup
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-neven-biskup.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>nbiskup@pdi.ucm.es</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>0034 91 394 3250</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Office</th>
                                        <td>110</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Juan Ignacio Beltrán Finez -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Dr. Juan Ignacio Beltrán Finez
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-juan-ignacio.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>juanbelt@ucm.es</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>91394 5220</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Links</th>
                                        <td>
                                            <a href="http://www.researchgate.net/profile/Juan_Beltran4" target="_blank">ResearchGate</a> &nbsp;
                                            <a href="http://www.researcherid.com/rid/I-8526-2012" target="_blank">ResearcherID</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <h2 class="add-margin-bottom add-margin-top-big">Visiting Researcher</h2>

            <!-- Diego Arias -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Dr. Diego Arias
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <i class="text-muted">No photo available</i>
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>91394 5220</td>
                                    </tr>
                                    <tr>
                                        <th scopre="row">Address</th>
                                        <td>Universidad del Quindío, Armenia, Colombia</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="add-margin-bottom add-margin-top-big">PhD and Master Students</h2>

            <!-- Fernando Gallego -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Fernando Gallego Toledo
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-fernando-gallego.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>91394 5247</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>fernadogallego@ucm.es</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Affiliation</th>
                                        <td>
                                            <i>Laboratorio de heteroestructuras con aplicación en espintrónica</i>, UCM/CSIC,
                                            Sor Juana Inés de la Cruz, 3,Cantoblanco, 28049 Madrid.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- David Hernández -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; David Hernández Martín
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-david-hernandez.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>0034 91 394 4435</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Office</th>
                                        <td>114.0 (Library)</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Gloria Orfila -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; Gloria Orfila Rodriguez
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-gloria-orfila.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>0034 91 394 4435</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>gorfila@ucm.es</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- David Sanchez -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plus-square"></i> &nbsp; David Sanchez Manzano
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="../../img/member-david-sanchez.jpg" class="img-thumbnail">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>0034 91 394 5247</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>davidsan@ucm.es</td>
                                    </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="add-margin-bottom add-margin-top-big">Former Group Members</h2>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a>
                        Pedro Schio
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            LNLS, Campinas, São Paulo, Brasil
                        </em>
                    </span>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a>
                        Mirko Rocci
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            Francis Bitter Magnet Laboratory and Plasma Science and Fusion Center, Massachusetts Institute of Technology, Cambridge USA
                        </em>
                    </span>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a>
                        Gabriel Sanchez
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            University of Tokyo, Japan
                        </em>
                    </span>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a>
                        Dr. Flavio Y. Bruno
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            University of Geneva
                        </em>
                    </span>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="http://www.trt.thalesgroup.com/ump-cnrs-thales/phonebook/visani.htm">
                        Dr. Cristina Visani
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            Postdoctoral Researcher (Unité Mixte de Physique CNRS/Thales, Palaiseau Cedex, France)
                        </em>
                    </span>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="http://www.trt.thalesgroup.com/ump-cnrs-thales/phonebook/villegas.htm">
                        Dr. Javier Villegas
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            Principal Investigator (Unité Mixte de Physique CNRS/Thales, Palaiseau Cedex, France)
                        </em>
                    </span>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a>
                        Dr. Vanessa Peña
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            Investigator, imec semiconductors, Leuven Belgium
                        </em>
                    </span>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a>
                        Dr. Javier Garcia Barriocanal
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            Research Associate, College of Science & Engineering, University of Minnesota
                        </em>
                    </span>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a>
                        Dr. Juan Salafranca Laforga
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            BBVA Compass, Atlanta
                        </em>
                    </span>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a>
                        Roberto de Andrés
                    </a>
                    <br/>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a>
                        Dr. Javier Grandal Quintana
                    </a>
                    <br/>
                    <span class="text-muted">
                        <em>
                            ETSIT Universidad Politecnica, Madrid
                        </em>
                    </span>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a>
                        Ana Pérez Muñoz
                    </a>
                    <br/>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <a>
                        Mariona Cabero Piris
                    </a>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $(".panel-heading").click(function () {
            $(this).siblings(".panel-body").slideToggle();
        })
    });

</script>

<?php require_once "inc/footer.html"?>