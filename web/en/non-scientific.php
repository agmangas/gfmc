<?php require_once "inc/header.html";?>
<?php require_once "inc/navigation.html";?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>Non-scientific Activities</h1>
                    <hr class="small">
                    <span class="subheading">Physics of Complex Materials Group</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container non-scientific">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-sm-6">
                    <div class="thumbnail">
                        <a class="fancybox" rel="group" href="../../img/tesis-mirko-raith.jpg">
                            <img src="../../img/tesis-mirko-raith.jpg">
                        </a>

                        <div class="caption">
                            Celebrating 2016
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="thumbnail">
                        <a class="fancybox" rel="group" href="../../img/non-scientific-barbecue.jpg">
                            <img src="../../img/non-scientific-barbecue-thumb.jpg">
                        </a>

                        <div class="caption">
                            Barbecue 2014
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="thumbnail">
                        <a class="fancybox" rel="group" href="../../img/non-scientific-group-orange.jpg">
                            <img src="../../img/non-scientific-group-orange-thumb.jpg">
                        </a>

                        <div class="caption">
                            Group 2014
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="thumbnail">
                        <a class="fancybox" rel="group" href="../../img/non-scientific-pinilla-01.jpg">
                            <img src="../../img/non-scientific-pinilla-01-thumb.jpg">
                        </a>

                        <div class="caption">
                            Skiing La Pinilla 2013
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="thumbnail">
                        <a class="fancybox" rel="group" href="../../img/non-scientific-pinilla-02.jpg">
                            <img src="../../img/non-scientific-pinilla-02-thumb.jpg">
                        </a>

                        <div class="caption">
                            Skiing La Pinilla 2013
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="thumbnail">
                        <a class="fancybox" rel="group" href="../../img/non-scientific-pinilla-03.jpg">
                            <img src="../../img/non-scientific-pinilla-03-thumb.jpg">
                        </a>

                        <div class="caption">
                            Skiing La Pinilla 2013
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="thumbnail">
                        <a class="fancybox" rel="group" href="../../img/non-scientific-mallorca-01.jpg">
                            <img src="../../img/non-scientific-mallorca-01-thumb.jpg">
                        </a>

                        <div class="caption">
                            Mallorca 2012
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $(".fancybox").fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            nextEffect: 'none',
            prevEffect: 'none',
            maxHeight: 700
        });
    });

</script>

<?php require_once "inc/footer.html"?>