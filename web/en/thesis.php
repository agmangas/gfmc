<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Thesis</h1>
                        <hr class="small">
                        <span class="subheading">Physics of Complex Materials Group</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
    <div class="row">
    <div class="col-md-10 col-md-offset-1">

    <h2 class="add-margin-bottom">PhD Thesis</h2>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Javier Tornos Castillo &nbsp; <span class="text-muted">(2014)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Spin-dependent transport in oxide multiferroic tunnel junctions
            </em>
        </p>

        <a href="../../files/thesis/TesisJTornos.pdf"
           class="btn btn-default btn-sm add-margin-top-small"
           target="_blank"><i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
    </div>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Fabián Cuéllar &nbsp; <span class="text-muted">(2012)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Magnetic Tunnel Junctions Based on Complex Oxides
            </em>
        </p>
    </div>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Flavio Y. Bruno &nbsp; <span class="text-muted">(2011)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Electronic and Spin Reconstruction at Complex Oxide Interfaces
            </em>
        </p>

        <a href="../../files/thesis/TesisFBruno.pdf"
           class="btn btn-default btn-sm add-margin-top-small"
           target="_blank"><i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
    </div>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Cristina Visani &nbsp; <span class="text-muted">(2010)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Magnetism and spin dependent transport at interfaces between complex oxides
            </em>
        </p>

        <a href="../../files/thesis/TesisCVisani.pdf"
           class="btn btn-default btn-sm add-margin-top-small"
           target="_blank"><i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
    </div>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Javier García Barriocanal &nbsp; <span class="text-muted">(2007)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Efectos de Interfaces en Superrredes de Oxidos Complejos
            </em>
        </p>

        <a href="../../files/thesis/TesisJavierGarciaBarriocanal.pdf"
           class="btn btn-default btn-sm add-margin-top-small"
           target="_blank"><i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
    </div>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Vanessa Peña Hidalgo &nbsp; <span class="text-muted">(2006)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Estructura y Magnetotransporte de Interfases de Óxidos Complejos
            </em>
        </p>

        <a href="../../files/thesis/TesisVPena.pdf"
           class="btn btn-default btn-sm add-margin-top-small"
           target="_blank"><i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
    </div>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Alberto Rivera Calzada &nbsp; <span class="text-muted">(2003)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Movilidad iónica en conductores superiónicos: movilidad local, percolación y relajación
                vibracional
            </em>
        </p>

        <a href="../../files/thesis/TesisRiveraCalzada.pdf"
           class="btn btn-default btn-sm add-margin-top-small"
           target="_blank"><i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
    </div>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            María Varela del Arco &nbsp; <span class="text-muted">(2001)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Crecimiento y caracterización de superredes basadas en Superconductores de Alta Temperatura
                Crítica: relación entre microestructura y propiedades.
            </em>
        </p>

        <a href="../../files/thesis/TesisVarelaDelArco.pdf"
           class="btn btn-default btn-sm add-margin-top-small"
           target="_blank"><i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
    </div>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Carlos León Yebra &nbsp; <span class="text-muted">(1997)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Relajación de la conductividad eléctrica en conductores iónicos cristalinos
            </em>
        </p>

        <a href="../../files/thesis/TesisLeonYebra.pdf"
           class="btn btn-default btn-sm add-margin-top-small"
           target="_blank"><i class="fa fa-file-pdf-o"></i> &nbsp; PDF</a>
    </div>

    <h2 class="add-margin-bottom add-margin-top-big">Master Thesis</h2>


    <!--
    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Gabriel Sánchez
        </h4>
    </div>
    -->

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Roberto de Andres &nbsp; <span class="text-muted">(2014)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Electrorresistencia en uniones basadas en óxidos complejos
            </em>
        </p>
    </div>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            David Hernández &nbsp; <span class="text-muted">(2012)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Caracterización dieléctrica de películas delgadas ferroeléctricas
            </em>
        </p>
    </div>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Mariona Cabero &nbsp; <span class="text-muted">(2012)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Caracterización estructural y de transporte de películas ultradelgadas de conductores
                iónicos
            </em>
        </p>
    </div>

    <hr/>

    <div class="thesis-entry">
        <h4 class="paper-title">
            Ana Pérez &nbsp; <span class="text-muted">(2012)</span>
        </h4>

        <p class="small-margin-p">
            <em>
                Dopado electrostático en sistemas fuertemente correlacionados
            </em>
        </p>
    </div>

    </div>
    </div>
    </div>


<?php require_once("inc/footer.html") ?>