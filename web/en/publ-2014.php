<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">2014</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div>
                    <h4 class="paper-title">
                        Resistive switching in manganite/graphene hybrid planar nanostructures
                    </h4>

                    <div class="well add-margin-top">
                        <b>Author(s)</b> &nbsp; Mirko Rocci, Javier Tornos, Alberto Rivera-Calzada, Zouhair Sefrioui,
                        Marta Clement, Enrique Iborra, Carlos Leon, Jacobo Santamaria<br/>
                        <b>Source</b> &nbsp; Appl. Phys. Lett. 104, 102408 (2014)<br/>
                    </div>
                </div>
                <hr/>
                <div>
                    <h4 class="paper-title">
                        Competition between Covalent Bonding and Charge Transfer at Complex-Oxide Interfaces
                    </h4>

                    <div class="well add-margin-top">
                        <b>Author(s)</b> &nbsp; Juan Salafranca, Julián Rincón, Javier Tornos, Carlos León, Jacobo
                        Santamaria, Elbio Dagotto, Stephen J. Pennycook, Maria Varela<br/>
                        <b>Source</b> &nbsp; Phys. Rev. Lett. 112, 196802 (2014)<br/>
                    </div>
                </div>
                <hr/>
                <div>
                    <h4 class="paper-title">
                        Reversible electric-field control of magnetization at oxide interfaces
                    </h4>

                    <div class="well add-margin-top">
                        <b>Author(s)</b> &nbsp; F. A. Cuellar, Y. H. Liu, J. Salafranca, N. Nemes, E. Iborra, G.
                        Sanchez-Santolino, M. Varela, M. Garcia Hernandez, J. W. Freeland, M. Zhernenkov, M. R.
                        Fitzsimmons, S. Okamoto, S. J. Pennycook, M. Bibes, A. Barthélémy, S. G. E. te Velthuis, Z.
                        Sefrioui, C. Leon, J. Santamaria<br/>
                        <b>Source</b> &nbsp; Nature Commun. 5:4215 (2014)<br/>
                        <b>DOI</b> &nbsp; 10.1038/ncomms5215
                    </div>
                </div>
                <hr/>
                <div>
                    <h4 class="paper-title">
                        Oxygen Octahedral Distortions in LaMO3/SrTiO3 Superlattices
                    </h4>

                    <div class="well add-margin-top">
                        <b>Author(s)</b> &nbsp; Gabriel Sanchez-Santolino, Mariona Cabero, Maria Varela, Javier
                        Garcia-Barriocanal, Carlos Leon, Stephen J. Pennycook, Jacobo Santamaria<br/>
                        <b>Source</b> &nbsp; Microsc. Microanal. 20, 825 (2014)<br/>
                    </div>
                </div>
                <hr/>
                <div>
                    <h4 class="paper-title">
                        A systematic study of Nasicon-type Li1+xMxTi2−x(PO4)3 (M: Cr, Al, Fe) by neutron diffraction and
                        impedance spectroscopy
                    </h4>

                    <div class="well add-margin-top">
                        <b>Author(s)</b> &nbsp; M. Pérez-Estébanez, J. Isasi-Marín, D.M. Többens, A. Rivera-Calzada, C.
                        León<br/>
                        <b>Source</b> &nbsp; Solid State Ionics 266, 1 (2014)<br/>
                    </div>
                </div>
                <hr/>
                <div>
                    <h4 class="paper-title" id="nemes-et-al-advmat-2014">
                        Signatures of a two-dimensional ferromagnetic electron gas at the La0.7Sr0.3MnO3/SrTiO3
                        interface arising from orbital reconstruction
                    </h4>

                    <div class="well add-margin-top">
                        <b>Author(s)</b> &nbsp; Norbert Nemes; Maria Jose Calderon; Juan Ignacio Beltran; Flavio Bruno;
                        Javier Garcia-Barriocanal; Zouhair Sefrioui; Carlos Leon; Mar Garcia-Hernandez; Carmen Muñoz;
                        Luis Brey; Jacobo Santamaria<br/>
                        <b>Source</b> &nbsp; Adv. Mater. (2014)<br/>
                        <b>DOI</b> &nbsp; 10.1002/adma.201402829
                    </div>

                    <a href="http://dx.doi.org/10.1002/adma.201402829" class="btn btn-default btn-sm" target="_blank">
                        Link dx.doi.org
                    </a>
                </div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>