<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">2012</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">Chemical synthesis of oriented ferromagnetic LaSr-2 x 4 manganese oxide
                        molecular sieve nanowires</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Carretero-Genevrier, Adrian; Gazquez,
                        Jaume; Magen, Cesar; et al.<br/><b>Source</b> &nbsp; Chemical Communications Volume: 48 Issue:
                        50 Pages: 6223-6225 Published: 2012<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp;
                        10.1039/c2cc31367g
                    </div>
                    <a href="http://dx.doi.org/10.1039/c2cc31367g" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/CarreteroGenevrieretalChemCommun482012p6223.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Effect of Interface-Induced Exchange Fields on Cuprate-Manganite Spin
                        Switches</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Liu, Yaohua; Visani, C.; Nemes, N. M.; et
                        al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 108 Issue: 20 Published: MAY 18
                        2012<br/><b>Times Cited</b> &nbsp; 5<br/><b>DOI</b> &nbsp; 10.1103/PhysRevLett.108.207205
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevLett.108.207205" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/LiuetalPhysRevLett1082012p207205.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Effects of sintering temperature on the internal barrier layer capacitor
                        (IBLC) structure in CaCu3Ti4O12 (CCTO) ceramics</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Schmidt, Rainer; Stennett, Martin C.;
                        Hyatt, Neil C.; et al.<br/><b>Source</b> &nbsp; Journal of the European Ceramic Society Volume:
                        32 Issue: 12 Pages: 3313-3323 Published: SEP 2012<br/><b>Times Cited</b> &nbsp;
                        13<br/><b>DOI</b> &nbsp; 10.1016/j.jeurceramsoc.2012.03.040<br/><br/></div>
                    <a href="http://dx.doi.org/10.1016/j.jeurceramsoc.2012.03.040" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/RSchmidtetalJEurCeramSoc322012p3313.pdf"
                        class="btn btn-default" target="_blank">PDF</a>&nbsp;<a
                        href="../../files/publications/2012/RSchmidtetalJEurCeramSoc322012p3313supportinginformation.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Electric characterization of grain boundaries in ionic conductors by
                        impedance spectroscopy measurements in a bicrystal</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Frechero, M. A.; Rocci, M.; Schmidt,
                        Rainer; et al.<br/><b>Source</b> &nbsp; Boletin De La Sociedad Espanola De Ceramica Y Vidrio
                        Volume: 51 Issue: 1 Pages: 13-18 Published: JAN-FEB 2012<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b>
                        &nbsp; 10.3989/cyv.032012
                    </div>
                    <a href="http://dx.doi.org/10.3989/cyv.032012" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/MAFrecheroetalBolSocEspCermVidrio512012p13.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Equal-spin Andreev reflection and long-range coherent transport in
                        high-temperature superconductor/half-metallic ferromagnet junctions</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Visani, C.; Sefrioui, Z.; Tornos, J.; et
                        al.<br/><b>Source</b> &nbsp; Nature Physics Volume: 8 Issue: 7 Pages: 539-543 Published: JUL
                        2012<br/><b>Times Cited</b> &nbsp; 20<br/><b>DOI</b> &nbsp; 10.1038/nphys2318
                    </div>
                    <a href="http://dx.doi.org/10.1038/nphys2318" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a href="../../files/publications/2012/VisanietalNatPhys82012p539.pdf"
                                               class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Ferroelectric substrate effects on the magnetism, magnetotransport, and
                        electroresistance of La0.7Ca0.3MnO3 thin films on BaTiO3</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Alberca, A.; Munuera, C.; Tornos, J.; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 86 Issue: 14 Published: OCT 24
                        2012<br/><b>Times Cited</b> &nbsp; 6<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.86.144416
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.86.144416" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/AlbercaetalPhysRevB862012p144416.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Increased ionic conductivity in microwave hydrothermally synthesized
                        rare-earth doped ceria Ce1-xRExO2-(x/2)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Prado-Gonjal, Jesus; Schmidt, Rainer;
                        Espindola-Canuto, Jesus; et al.<br/><b>Source</b> &nbsp; Journal of Power Sources Volume: 209
                        Pages: 163-171 Published: JUL 1 2012<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp;
                        10.1016/j.jpowsour.2012.02.082
                    </div>
                    <a href="http://dx.doi.org/10.1016/j.jpowsour.2012.02.082" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/JPradoGonjaletalJPowerSources2092012p163.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Low Temperature Epitaxial Oxide Ultrathin Films and Nanostructures by
                        Atomic Layer Deposition</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Coll, Mariona; Gazquez, Jaume; Palau, Anna;
                        et al.<br/><b>Source</b> &nbsp; Chemistry of Materials Volume: 24 Issue: 19 Pages: 3732-3737
                        Published: OCT 9 2012<br/><b>Times Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp; 10.1021/cm301864c
                    </div>
                    <a href="http://dx.doi.org/10.1021/cm301864c" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a href="../../files/publications/2012/ColletalChemMater242012p3732.pdf"
                                               class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Magnetite (Fe3O4): a new variant of relaxor multiferroic?</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ziese, M.; Esquinazi, P. D.; Pantel, D.; et
                        al.<br/><b>Source</b> &nbsp; Journal of Physics-Condensed Matter Volume: 24 Issue: 8 Published:
                        FEB 29 2012<br/><b>Times Cited</b> &nbsp; 6<br/><b>DOI</b> &nbsp; 10.1088/0953-8984/24/8/086007
                    </div>
                    <a href="http://dx.doi.org/10.1088/0953-8984/24/8/086007" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/ZieseetalJPhysCondensMatter242012p086007.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Magnetoimpedance spectroscopy of epitaxial multiferroic thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Schmidt, Rainer; Ventura, Jofre;
                        Langenberg, Eric; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 86 Issue: 3
                        Published: JUL 10 2012<br/><b>Times Cited</b> &nbsp; 11<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevB.86.035113<br/></div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.86.035113" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/RSchmidtetalPhysRevB862012p035113.pdf"
                        class="btn btn-default" target="_blank">PDF</a>&nbsp;<a
                        href="../../files/publications/2012/RSchmidtetalPhysRevB862012p035113SupportingInformation.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Measurement of Magnetic Exchange in Ferromagnet-Superconductor
                        La2/3Ca1/3MnO3/YBa2Cu3O7 Bilayers</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Giblin, S. R.; Taylor, J. W.; Duffy, J. A.;
                        et al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 109 Issue: 13 Published: SEP 28
                        2012<br/><b>Times Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp; 10.1103/PhysRevLett.109.137005
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevLett.109.137005" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/GiblinetalPhysRevLett1092012p137005.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Pulsed laser deposition growth of heteroepitaxial
                        YBa2Cu3O7/La0.67Ca0.33MnO3 superlattices on NdGaO3 and Sr0.7La0.3Al0.65Ta0.35O3 substrates</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Malik, V. K.; Marozau, I.; Das, S.; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 85 Issue: 5 Published: FEB 27
                        2012<br/><b>Times Cited</b> &nbsp; 9<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.85.054514
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.85.054514" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/MaliketalPhysRevB852012p054514.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Structural and physical properties of microwave synthesized orthorhombic
                        perovskite erbium chromite ErCrO(3)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Prado-Gonjal, Jesus; Schmidt, Rainer;
                        Avila, David; et al.<br/><b>Source</b> &nbsp; Journal of the European Ceramic Society Volume: 32
                        Issue: 3 Pages: 611-618 Published: MAR 2012<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b>
                        &nbsp; 10.1016/j.jeurceramsoc.2011.10.002
                    </div>
                    <a href="http://dx.doi.org/10.1016/j.jeurceramsoc.2011.10.002" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/JPradoGonjaletalJEurCeramSoc322012p611.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Study of the nearly constant dielectric loss regime in ionic conductors
                        with pyrochlore-like structure</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Diaz-Guillen, M. R.; Fuentes, A. F.;
                        Diaz-Guillen, J. A.; et al.<br/><b>Source</b> &nbsp; Boletin De La Sociedad Espanola De Ceramica
                        Y Vidrio Volume: 51 Issue: 1 Pages: 7-12 Published: JAN-FEB 2012<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b>
                        &nbsp; 10.3989/cyv.022012
                    </div>
                    <a href="http://dx.doi.org/10.3989/cyv.022012" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/DiazGuillenetalBolSocEspCeramVidrio512012p7.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Surfactant Organic Molecules Restore Magnetism in Metal-Oxide Nanoparticle
                        Surfaces</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Salafranca, Juan; Gazquez, Jaume; Perez,
                        Nicolas; et al.<br/><b>Source</b> &nbsp; Nano Letters Volume: 12 Issue: 5 Pages: 2499-2503
                        Published: MAY 2012<br/><b>Times Cited</b> &nbsp; 10<br/><b>DOI</b> &nbsp; 10.1021/nl300665z
                    </div>
                    <a href="http://dx.doi.org/10.1021/nl300665z" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a href="../../files/publications/2012/SalafrancaetalNanoLett1220122499.pdf"
                                               class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Testing the Elliott-Yafet spin-relaxation mechanism in KC8: A model system
                        of biased graphene</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Fabian, G.; Dora, B.; Antal, A.; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 85 Issue: 23 Published: JUN 4
                        2012<br/><b>Times Cited</b> &nbsp; 3<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.85.235405
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.85.235405" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/FabianetalPhysRevB852012p235405.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Thermally assisted tunneling transport in La0.7Ca0.3MnO3/SrTiO3:Nb
                        Schottky-like heterojunctions</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Cuellar, F. A.; Sanchez-Santolino, G.;
                        Varela, M.; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 85 Issue: 24 Published:
                        JUN 20 2012<br/><b>Times Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.85.245122
                    </div>
                    <a href="http://dx.doi.org/10.1103/PhysRevB.85.245122" class="btn btn-default btn-sm"
                       target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2012/CuellaretalPhysRevB852012p245122.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>