<?php require_once "inc/header.html";?>
<?php require_once "inc/navigation.html";?>

<?php

$papers = array(
    array(
        "title" => "Low thermal conductivity in La-filled cobalt antimonide skutterudites with an inhomogeneous filling factor prepared under high-pressure conditions",
        "fields" => array(
            "authors" => "F Serrano-Sánchez, J Prado-Gonjal, NM Nemes, N Biskup, M Varela, OJ Dura, JL Martínez, MT Fernández-Díaz, F Fauth, JA Alonso",
            "source" => "Journal of Materials Chemistry A 6 (1) 118-126, 2018.",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Structural evolution of Ge-doped SnSe thermoelectric material with low thermal conductivity",
        "fields" => array(
            "authors" => "Federico Serrano-Sanchez, Norbert Nemes, Jose Luis Martinez, Oscar Juan-Dura, Marco Antonio de la Torre, Maria Teresa Fernandez-Diaz and  Jose Antonio Alonso",
            "source" => "J. Appl. Cryst. 51, 337-343, 2018. ",
        ),
        "links" => array(),
    ),
);

?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <div class="page-heading">
          <h1>Publications</h1>
          <hr class="small">
          <span class="subheading">2018</span>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- Main Content -->
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <?php foreach ($papers as $paper): ?>
      <div>
        <h4 class="paper-title">
          <?php echo $paper["title"] ?>
        </h4>

        <div class="well add-margin-top">
          <?php foreach ($paper["fields"] as $key => $val): ?>
          <b><?php echo ucwords($key) ?></b> &nbsp;
          <?php echo $val ?>
          <br/>
          <?php endforeach;?>
        </div>

        <?php foreach ($paper["links"] as $link): ?>
        <a href="<?php echo $link["url"] ?>" class="btn btn-default btn-sm" target="_blank">
          Link
          <?php echo $link["name"] ?>
        </a>
        <?php endforeach;?>
      </div>
      <hr/>
      <?php endforeach;?>
    </div>
  </div>
</div>

<?php require_once "inc/footer.html"?>