<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">2007</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">Effect of La substitution for Gd in the ionic conductivity and oxygen
                        dynamics of fluorite-type Gd2Zr2O7</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Diaz-Guillen, J. A.; Diaz-Guillen, M. R.;
                        Almanza, J. M.; et al.<br/><b>Source</b> &nbsp; Journal of Physics-Condensed Matter Volume: 19
                        Issue: 35 Published: SEP 5 2007<br/><b>Times Cited</b> &nbsp; 24<br/><b>DOI</b> &nbsp;
                        10.1088/0953-8984/19/35/356212
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000248783000016"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1088/0953-8984/19/35/356212" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Electrical conductivity relaxation in lithium doped silver iodide</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Correa, H.; Vargas, R. A.;
                        Garcia-Barriocanal, J.; et al.<br/><b>Source</b> &nbsp; Journal of the European Ceramic Society
                        Volume: 27 Issue: 13-15 Pages: 4297-4300 Published: 2007<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b>
                        &nbsp; 10.1016/j.jeurceramsoc.2007.02.148
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000248822800154"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jeurceramsoc.2007.02.148" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Influence of structural disorder on the dynamics of mobile oxygen ions in
                        dy(2)(Ti1-yZry)(2)O-7</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Moreno, Karla J.; Fuentes, Antonio F.;
                        Amador, Ulises; et al.<br/><b>Source</b> &nbsp; Journal of Non-Crystalline Solids Volume: 353
                        Issue: 41-43 Pages: 3947-3955 Published: NOV 1 2007<br/><b>Times Cited</b> &nbsp;
                        4<br/><b>DOI</b> &nbsp; 10.1016/j.jnoncrysol.2007.04.037
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000250491700023"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jnoncrysol.2007.04.037" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Influence of thermally induced oxygen order on mobile ion dynamics in
                        Gd-2(Ti0.65Zr0.35)(2)O-7</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Moreno, Karla J.; Fuentes, Antonio F.;
                        Maczka, Miroslaw; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 75 Issue: 18
                        Published: MAY 2007<br/><b>Times Cited</b> &nbsp; 19<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevB.75.184303
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000246890600033"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.75.184303" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Intermediate rotator phase in lead(II) alkanoates</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Casado, F. J. Martinez; Perez, M. V.
                        Garcia; Yelamos, M. I. Redondo; et al.<br/><b>Source</b> &nbsp; Journal of Physical Chemistry C
                        Volume: 111 Issue: 18 Pages: 6826-6831 Published: MAY 10 2007<br/><b>Times Cited</b> &nbsp;
                        11<br/><b>DOI</b> &nbsp; 10.1021/jp068823j
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000246190500035"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1021/jp068823j" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Magnetoresistance in La(0.7)Ca(0.3)MnO(3)-YBa(2)Cu(3)O(7)F/S/F
                        trilayers</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V.; Visani, C.; Bruno, F.; et
                        al.<br/><b>Source</b> &nbsp; Journal of Magnetism and Magnetic Materials Volume: 316 Issue: 2
                        Pages: E745-E748 Published: SEP 2007<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp;
                        10.1016/j.jmmm.2007.03.082
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000248150000301"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jmmm.2007.03.082" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Precipitation of Cr-rich phases in rapidly solidified Ni-20Al-8Cr (at.%)
                        powders</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Garcia Barriocanal, J.; Perez, P.; Garces,
                        G.; et al.<br/><b>Source</b> &nbsp; Intermetallics Volume: 15 Issue: 8 Pages: 1096-1104
                        Published: AUG 2007<br/><b>Times Cited</b> &nbsp; 6<br/><b>DOI</b> &nbsp;
                        10.1016/j.intermet.2007.01.002
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000248065300016"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.intermet.2007.01.002" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Spectroscopic imaging of oxide interfaces with aberration corrected
                        probes</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Varela, M.; Oxley, M. P.; Roberts, K.
                        Griffin; et al.<br/><b>Source</b> &nbsp; Microscopy and Microanalysis Volume: 13 Pages: 142-143
                        Published: 2007<br/><b>Times Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp; 10.1017/S1431927607072054
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000258691300071"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1017/S1431927607072054" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Spin dependent transport at oxide La(0.7)Ca(0.3)MnO(3)/YBa(2)CU(3)O(7)
                        ferromagnet/superconductor interfaces</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V.; Nemes, N.; Visani, C.; et
                        al.<br/><b>Source</b> &nbsp; Journal of the European Ceramic Society Volume: 27 Issue: 13-15
                        Pages: 3967-3970 Published: 2007<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp;
                        10.1016/j.jeurceramsoc.2007.02.076
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000248822800084"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jeurceramsoc.2007.02.076" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Spin-dependent magnetoresistance of ferromagnet/superconductor/ferromagnet
                        La0.7Ca0.3MnO3/YBa2Cu3O7-delta/La0.7Ca0.3MnO3 trilayers</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Visani, C.; Pena, V.; Garcia-Barriocanal,
                        J.; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 75 Issue: 5 Published: FEB
                        2007<br/><b>Times Cited</b> &nbsp; 32<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.75.054501
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000244532600071"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.75.054501" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Structural characterization and ionic conductivity of metastable
                        Gd2(Ti0.65Zr0.35)2O7 powders prepared by mechanical milling</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Fuentes, Antonio F.; Moreno, Karla J.;
                        Santamaria, Jacobo; et al.<br/><b>Source</b> &nbsp; Solid-State Ionics-2006 Volume: 972 Pages:
                        97-102 Published: 2007<br/><b>Times Cited</b> &nbsp; 2
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000246435400013"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>