<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">2013</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">Microwave-Assisted Synthesis, Microstructure, and Physical Properties of
                        Rare-Earth Chromites</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Prado-Gonjal, Jesus; Schmidt, Rainer;
                        Romero, Juan-Jose; et al.<br/><b>Source</b> &nbsp; Inorganic Chemistry Volume: 52 Issue: 1
                        Pages: 313-320 Published: JAN 7 2013<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp;
                        10.1021/ic302000j
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000313220500035"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1021/ic302000j" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2013/JPradoGonjaletalInorgChem522013p313.pdf"
                        class="btn btn-default" target="_blank">PDF</a>&nbsp;<a
                        href="../../files/publications/2013/JPradoGonjaletalInorgChem522013p313SupportingInformation.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Dynamics of interacting oxygen ions in yttria stabilized zirconia: bulk
                        material and nanometer thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ngai, K. L.; Santamaria, J.; Leon,
                        Carlos<br/><b>Source</b> &nbsp; European Physical Journal B Volume: 86 Issue: 1 Published: JAN
                        2013<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp; 10.1140/epjb/e2012-30737-2
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000314287200007"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1140/epjb/e2012-30737-2" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2013/NgaietalEurPhysJB862013p7.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Electron Doping by Charge Transfer at LaFeO3/Sm2CuO4 Epitaxial
                        Interfaces</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Bruno, Flavio Y.; Schmidt, Rainer; Varela,
                        Maria; et al.<br/><b>Source</b> &nbsp; Advanced Materials Volume: 25 Issue: 10 Pages: 1468-1473
                        Published: MAR 13 2013<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp;
                        10.1002/adma.201203483
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000316007200015"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1002/adma.201203483" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a href="../../files/publications/2013/FYBrunoetalAdvMater252013p1468.pdf"
                                               class="btn btn-default" target="_blank">PDF</a>&nbsp;<a
                        href="../../files/publications/2013/FYBrunoetalAdvMater252013p1468SupportingInformation.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Strain-driven broken twin boundary coherence in YBa2Cu3O7-delta
                        nanocomposite thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Guzman, R.; Gazquez, J.; Rouco, V.; et
                        al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 102 Issue: 8 Published: FEB 25 2013<br/><b>Times
                            Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp; 10.1063/1.4793749
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000315597000038"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.4793749" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2013/GuzmanetalApplPhysLett1022013p081906.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Ultra-thin filaments revealed by the dielectric response across the
                        metal-insulator transition in VO2</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Ramirez, J. -G.; Schmidt, Rainer; Sharoni,
                        A.; et al.<br/><b>Source</b> &nbsp; Applied Physics Letters Volume: 102 Issue: 6 Published: FEB
                        11 2013<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp; 10.1063/1.4792052
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000315053300081"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.4792052" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2013/JGRamirezetalApplPhysLett1022013p063110.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Characterization of surface metallic states in SrTiO3 by means of
                        aberration corrected electron microscopy</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sanchez-Santolino, G.; Tornos, J.; Bruno,
                        F. Y.; et al.<br/><b>Source</b> &nbsp; Ultramicroscopy Volume: 127 Pages: 109-113 Published: APR
                        2013<br/><b>Times Cited</b> &nbsp; 2<br/><b>DOI</b> &nbsp; 10.1016/j.ultramic.2012.07.013
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000316659100016"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.ultramic.2012.07.013" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2013/SanchezSantolinoetalUltramicroscopy1272013p109.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Disorder-controlled superconductivity at YBa2Cu3O7/SrTiO3 interfaces</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Garcia-Barriocanal, J.; Perez-Munoz, A. M.;
                        Sefrioui, Z.; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 87 Issue: 24 Published:
                        JUN 11 2013<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp;
                        10.1103/PhysRevB.87.245105<br/></div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000320166300002"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.87.245105" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2013/GarciaBarriocanaletalPhysRevB872013p245105.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Emergent Spin Filter at the Interface between Ferromagnetic and Insulating
                        Layered Oxides</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Liu, Yaohua; Cuellar, F. A.; Sefrioui, Z.;
                        et al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 111 Issue: 24 Published: DEC 11
                        2013<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp; 10.1103/PhysRevLett.111.247203
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000328699800019"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevLett.111.247203" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2013/LiuetalPhysRevLett1112013p247203.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">INDUCED MAGNETISM AT OXIDE INTERFACES</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Santamaria, Jacobo; Garcia-Barriocanal,
                        Javier; Sefrioui, Zouhair; et al.<br/><b>Source</b> &nbsp; International Journal of Modern
                        Physics B Volume: 27 Issue: 19 Published: JUL 30 2013<br/><b>Times Cited</b> &nbsp;
                        2<br/><b>DOI</b> &nbsp; 10.1142/S0217979213300132
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000321828100001"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1142/S0217979213300132" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2013/SantamariaetalIntJModPhysB272013p1330013.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Influence of chromium content on the optical and electrical properties of
                        Li1+xCrxTi2-x(PO4)(3)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Perez-Estebanez, M.; Isasi-Marin, J.;
                        Diaz-Guerra, C.; et al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 241 Pages: 36-45
                        Published: JUN 15 2013<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp;
                        10.1016/j.ssi.2013.04.001
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000320146400007"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.ssi.2013.04.001" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2013/PerezEstebanezetalSolidStateIonics2412013p36.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Pressure dependence of superconducting critical temperature and upper
                        critical field of 2H-NbS2</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Tissen, V. G.; Osorio, M. R.; Brison, J.
                        P.; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 87 Issue: 13 Published: APR 3 2013<br/><b>Times
                            Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.87.134502
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000317097700003"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.87.134502" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2013/TissenetalPhysRevB872013p134502.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">XANES and EXAFS study of the local order in nanocrystalline
                        yttria-stabilized zirconia</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Dura, O. J.; Boada, R.; Lopez de la Torre,
                        M. A.; et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 87 Issue: 17 Published: MAY 22
                        2013<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.87.174109
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000319280200001"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.87.174109" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2013/DuraetalPhysRevB872013p174109.pdf" class="btn btn-default"
                        target="_blank">PDF</a></div>
                <hr/>
                <div>
                    <h4 class="paper-title">
                        Non-stoichiometry in "CaCu3Ti4O12" (CCTO) ceramics
                    </h4>

                    <div class="well add-margin-top">
                        <b>Author(s)</b> &nbsp; R. Schmidt, S. Pandey, P. Fiorenza, D.C. Sinclair<br/>
                        <b>Source</b> &nbsp; RSC Advances 3 (2013) p.14580
                    </div>
                </div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>