<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Research</h1>
                        <hr class="small">
                        <span class="subheading">Physics of Complex Materials Group</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row" id="research-row">
                    <div class="col-md-6">
                        <h2 class="add-margin-bottom">
                            Experimental Methods
                        </h2>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <ul class="no-margin-bottom">
                                    <li>Epitaxial growth of complex oxides</li>
                                    <li>Electron Beam Lithography</li>
                                    <li>Optical Lithography</li>
                                    <li>Advanced electron microscopy and spectroscopy techniques</li>
                                    <li>Magnetic Properties</li>
                                    <li>Dielectric Properties</li>
                                    <li>Magnetoresistance</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <h2 class="add-margin-bottom">
                            Research Lines
                        </h2>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <ul class="no-margin-bottom">
                                    <li>Epitaxial interfaces of complex oxides</li>
                                    <li>Magnetic Tunnel junctions</li>
                                    <li>High Tc Superconductors, proximity phenomena</li>
                                    <li>Oxide Nanostructures</li>
                                    <li>Ionically conducting oxide interfaces</li>
                                    <li>Ionic conductors for energy harvesting</li>
                                    <li>Dielectric spectroscopy of multiferroics</li>
                                    <li>Electron microscopy</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require_once("inc/footer.html") ?>