<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">2006</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">Atomic scale characterization of complex oxide interfaces</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Varela, Maria; Pennycook, Timothy J.; Tian,
                        Wei; et al.<br/><b>Source</b> &nbsp; Journal of Materials Science Volume: 41 Issue: 14 Pages:
                        4389-4393 Published: JUL 2006<br/><b>Times Cited</b> &nbsp; 11<br/><b>DOI</b> &nbsp;
                        10.1007/s10853-006-0150-4
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000239720900003"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1007/s10853-006-0150-4" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Comment on "Photoemission study of YBa2Cu3Oy thin films under light
                        illumination"</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Hoffmann, A.; Nieva, G.; Guimpel, J.; et
                        al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 97 Issue: 11 Published: SEP 15 2006<br/><b>Times
                            Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp; 10.1103/PhysRevLett.97.119701
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000240545600075"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevLett.97.119701" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Composition dependence of the dispersive nature of the ac conductivity in
                        ionic conductors Gd2Ti2-yZryO7 and Li0.5-xNaxLa0.5TiO3</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Garcia-Barriocanal, J.; Moreno, K. J.;
                        Fuentes, A. F.; et al.<br/><b>Source</b> &nbsp; Journal of Non-Crystalline Solids Volume: 352
                        Issue: 42-49 Pages: 5141-5146 Published: NOV 15 2006<br/><b>Times Cited</b> &nbsp;
                        1<br/><b>DOI</b> &nbsp; 10.1016/j.jnoncrysol.2006.04.028
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000242821800134"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jnoncrysol.2006.04.028" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Effects of structural microdomains on the vortex correlation length in
                        a-axis oriented EuBa2Cu3O7 thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Gonzalez, EM; Sefrioui, Z; Maiorov, B; et
                        al.<br/><b>Source</b> &nbsp; Journal of Physics and Chemistry of Solids Volume: 67 Issue: 1-3
                        Pages: 399-402 Published: JAN-MAR 2006<br/><b>Times Cited</b> &nbsp; 0<br/><b>DOI</b> &nbsp;
                        10.1016/j.jpcs.2005.10.093
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000236746600099"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jpcs.2005.10.093" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Evolution of mechanical properties with temperature in amorphous Mg10Ni10Y
                        ribbons</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Torrijos, M. A.; Garces, G.;
                        Garcia-Barriocanal, J.; et al.<br/><b>Source</b> &nbsp; Revista De Metalurgia Volume: 42 Issue:
                        1 Pages: 32-40 Published: JAN-FEB 2006<br/><b>Times Cited</b> &nbsp; 0
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000239834300004"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">High ionic conductivity of hydrated Li0.5FeOCl</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sagua, A; Rivera, A; Leon, C; et
                        al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 177 Issue: 11-12 Pages: 1099-1104
                        Published: APR 2006<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp;
                        10.1016/j.ssi.2006.03.031
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000238328100018"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.ssi.2006.03.031" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Interfaces on stage</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Santamaria, J<br/><b>Source</b> &nbsp;
                        Nature Physics Volume: 2 Issue: 4 Pages: 229-230 Published: APR 2006<br/><b>Times Cited</b>
                        &nbsp; 6<br/><b>DOI</b> &nbsp; 10.1038/nphys274
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000236979500010"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1038/nphys274" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Interfacially controlled transient photoinduced superconductivity</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V.; Gredig, T.; Santamaria, J.; et
                        al.<br/><b>Source</b> &nbsp; Physical Review Letters Volume: 97 Issue: 17 Published: OCT 27 2006<br/><b>Times
                            Cited</b> &nbsp; 13<br/><b>DOI</b> &nbsp; 10.1103/PhysRevLett.97.177005
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000241586800057"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevLett.97.177005" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Large magnetoresistance in oxide based ferromagnet/superconductor spin
                        switches</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V.; Nemes, N.; Visani, C.; et
                        al.<br/><b>Source</b> &nbsp; Degradation Processes in Nanostructured Materials Volume: 887
                        Pages: 95-102 Published: 2006<br/><b>Times Cited</b> &nbsp; 0
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000237235700012"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Mechanochemical synthesis and ionic conductivity in the
                        Gd-2(Sn1-yZry)(2)O-7 (0 <= y <= 1) solid solution</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Moreno, KJ; Fuentes, AF;
                        Garcia-Barriocanal, J; et al.<br/><b>Source</b> &nbsp; Journal of Solid State Chemistry Volume:
                        179 Issue: 1 Pages: 323-330 Published: JAN 2006<br/><b>Times Cited</b> &nbsp; 19<br/><b>DOI</b>
                        &nbsp; 10.1016/j.jssc.2005.09.036
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000234807500043"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jssc.2005.09.036" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Microstructure and mechanical properties of Ni3Al base alloy reinforced
                        with Cr particles produced by powder metallurgy</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Barriocanal, JG; Perez, P; Garces, G; et
                        al.<br/><b>Source</b> &nbsp; Intermetallics Volume: 14 Issue: 4 Pages: 456-463 Published: APR
                        2006<br/><b>Times Cited</b> &nbsp; 9<br/><b>DOI</b> &nbsp; 10.1016/j.intermet.2005.08.008
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000234093800014"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.intermet.2005.08.008" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Paramagnetic meissner effect in YBa2Cu3O7/La0.7Ca0.3MnO3 superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; de la Torre, MAL; Pena, V; Sefrioui, Z; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 73 Issue: 5 Published: FEB 2006<br/><b>Times
                            Cited</b> &nbsp; 19<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.73.052503
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000235668300019"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.73.052503" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Room-temperature synthesis and conductivity of the pyrochlore type
                        Dy-2(Ti1-yZry)(2)O-7 (0 <= y <= 1) solid solution</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Moreno, KJ; Guevara-Liceaga, MA; Fuentes,
                        AF; et al.<br/><b>Source</b> &nbsp; Journal of Solid State Chemistry Volume: 179 Issue: 3 Pages:
                        928-934 Published: MAR 2006<br/><b>Times Cited</b> &nbsp; 25<br/><b>DOI</b> &nbsp;
                        10.1016/j.jssc.2005.12.015
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000236085900039"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jssc.2005.12.015" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Spin diffusion versus proximity effect at ferromagnet/superconductor
                        La0.7Ca0.3MnO3/YBa2Cu3O7-delta interfaces</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V; Visani, C; Garcia-Barriocanal, J;
                        et al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 73 Issue: 10 Published: MAR
                        2006<br/><b>Times Cited</b> &nbsp; 32<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.73.104513
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000236467200115"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.73.104513" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Strain induced phase separation in La0.67Ca0.33MnO3 ultra thin films</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V; Sefrioui, Z; Arias, D; et al.<br/><b>Source</b>
                        &nbsp; Journal of Physics and Chemistry of Solids Volume: 67 Issue: 1-3 Pages: 472-475
                        Published: JAN-MAR 2006<br/><b>Times Cited</b> &nbsp; 5<br/><b>DOI</b> &nbsp;
                        10.1016/j.jpcs.2005.10.022
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000236746600118"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jpcs.2005.10.022" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Tunnel magnetoresistance in La0.7Ca0.3MnO3/PrBa2Cu3O7/La0.7Ca0.3MnO3</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Sefrioui, Z; Cros, V; Barthelemy, A; et al.<br/><b>Source</b>
                        &nbsp; Applied Physics Letters Volume: 88 Issue: 2 Published: JAN 9 2006<br/><b>Times Cited</b>
                        &nbsp; 9<br/><b>DOI</b> &nbsp; 10.1063/1.2162674
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000234606900053"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1063/1.2162674" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Vortex decoupling in LCMO/YBCO superlattices</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pena, V; Sefrioui, Z; Arias, D; et al.<br/><b>Source</b>
                        &nbsp; Journal of Physics and Chemistry of Solids Volume: 67 Issue: 1-3 Pages: 387-390
                        Published: JAN-MAR 2006<br/><b>Times Cited</b> &nbsp; 4<br/><b>DOI</b> &nbsp;
                        10.1016/j.jpcs.2005.10.090
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000236746600096"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jpcs.2005.10.090" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>