<?php require_once("inc/header.html"); ?>
<?php require_once("inc/navigation.html"); ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Publications</h1>
                        <hr class="small">
                        <span class="subheading">2009</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div><h4 class="paper-title">Aberration-corrected scanning transmission electron microscopy: from atomic
                        imaging and analysis to solving energy problems</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Pennycook, S. J.; Chisholm, M. F.; Lupini,
                        A. R.; et al.<br/><b>Source</b> &nbsp; Philosophical Transactions of the Royal Society
                        a-Mathematical Physical and Engineering Sciences Volume: 367 Issue: 1903 Pages: 3709-3733
                        Published: SEP 28 2009<br/><b>Times Cited</b> &nbsp; 23<br/><b>DOI</b> &nbsp;
                        10.1098/rsta.2009.0112
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000269569400006"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1098/rsta.2009.0112" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2009/PennycooketalPhilTransRSocA3672009p3709.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Dielectric relaxation of CsHSeO(4) above room temperature</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Checa, O.; Diosa, J. E.; Vargas, R. A.; et
                        al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 180 Issue: 9-10 Pages: 673-676
                        Published: MAY 29 2009<br/><b>Times Cited</b> &nbsp; 1<br/><b>DOI</b> &nbsp;
                        10.1016/j.ssi.2009.03.010
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000266838600003"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.ssi.2009.03.010" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2009/ChecaetalSolidStateIonics1802009p673.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Dynamics of mobile oxygen ions in disordered pyrochlore-type oxide-ion
                        conductors</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Diaz-Guillen, M. R.; Moreno, K. J.;
                        Diaz-Guillen, J. A.; et al.<br/><b>Source</b> &nbsp; Diffusion in Materials - Dimat2008 Volume:
                        289-292 Pages: 347-354 Published: 2009<br/><b>Times Cited</b> &nbsp; 2
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000270899300040"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Evidence from magnetoresistance measurements for an induced triplet
                        superconducting state in La(0.7)Ca(0.3)MnO(3)/YBa(2)Cu(3)O(7-delta) multilayers</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Hu, T.; Xiao, H.; Visani, C.; et
                        al.<br/><b>Source</b> &nbsp; Physical Review B Volume: 80 Issue: 6 Published: AUG 2009<br/><b>Times
                            Cited</b> &nbsp; 8<br/><b>DOI</b> &nbsp; 10.1103/PhysRevB.80.060506
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000269638800015"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1103/PhysRevB.80.060506" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Identifying the electron spin resonance of conduction electrons in alkali
                        doped SWCNTs</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Galambos, M.; Fabian, G.; Simon, F.; et al.<br/><b>Source</b>
                        &nbsp; Physica Status Solidi B-Basic Solid State Physics Volume: 246 Issue: 11-12 Pages:
                        2760-2763 Published: DEC 2009<br/><b>Times Cited</b> &nbsp; 6<br/><b>DOI</b> &nbsp;
                        10.1002/pssb.200982330
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000272904100080"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1002/pssb.200982330" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2009/GalambosetalPhysStatusSolidiB2462009p2760.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Li mobility in Li(0.5-x)Na(x)La(0.5)TiO(3) perovskites (0 <= x <= 0.5)
                        Influence of structural and compositional parameters</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Jimenez, R.; Rivera, A.; Varez, A.; et
                        al.<br/><b>Source</b> &nbsp; Solid State Ionics Volume: 180 Issue: 26-27 Pages: 1362-1371
                        Published: OCT 19 2009<br/><b>Times Cited</b> &nbsp; 6<br/><b>DOI</b> &nbsp;
                        10.1016/j.ssi.2009.08.002
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000271985600004"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.ssi.2009.08.002" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2009/JimenezetalSolidStateIonics1802009p1362.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Magnetic coupling in
                        La(0.7)Ca(0.3)MnO(3)/YBa(2)Cu(3)O(7)/La(0.7)Ca(0.3)MnO(3) trilayers</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Nemes, N. M.; Visani, C.;
                        Garcia-Barriocanal, J.; et al.<br/><b>Source</b> &nbsp; Diffusion in Materials - Dimat2008
                        Volume: 289-292 Pages: 303-309 Published: 2009<br/><b>Times Cited</b> &nbsp; 0
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000270899300034"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a></div>
                <hr/>
                <div><h4 class="paper-title">Many-ion Dynamics: The Common View of CM and MC</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Leon, C.; Habasaki, J.; Ngai, K. L.<br/><b>Source</b>
                        &nbsp; Zeitschrift Fur Physikalische Chemie-International Journal of Research in Physical
                        Chemistry & Chemical Physics Volume: 223 Issue: 10-11 Pages: 1311-1325 Published: 2009<br/><b>Times
                            Cited</b> &nbsp; 5<br/><b>DOI</b> &nbsp; 10.1524/zpch.2009.6081
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000273163300013"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1524/zpch.2009.6081" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">Response to Comment on "Colossal Ionic Conductivity at Interfaces of
                        Epitaxial ZrO(2):Y(2)O(3)/SrTiO(3) Heterostructures"</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Garcia-Barriocanal, J.; Rivera-Calzada, A.;
                        Varela, M.; et al.<br/><b>Source</b> &nbsp; Science Volume: 324 Issue: 5926 Published: APR 24
                        2009<br/><b>Times Cited</b> &nbsp; 12<br/><b>DOI</b> &nbsp; 10.1126/science.1169018
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000265411200026"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1126/science.1169018" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2009/GarciaBarriocanaletalScience3242009p465b.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
                <hr/>
                <div><h4 class="paper-title">Tailoring Disorder and Dimensionality: Strategies for Improved Solid Oxide
                        Fuel Cell Electrolytes</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Garcia-Barriocanal, Javier; Rivera-Calzada,
                        Alberto; Varela, Maria; et al.<br/><b>Source</b> &nbsp; Chemphyschem Volume: 10 Issue: 7 Pages:
                        1003-1011 Published: MAY 5 2009<br/><b>Times Cited</b> &nbsp; 19<br/><b>DOI</b> &nbsp;
                        10.1002/cphc.200800691
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000266292000001"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1002/cphc.200800691" class="btn btn-default btn-sm" target="_blank">Link
                        dx.doi.org</a></div>
                <hr/>
                <div><h4 class="paper-title">The effect of homovalent A-site substitutions on the ionic conductivity of
                        pyrochlore-type Gd(2)Zr(2)O(7)</h4>

                    <div class="well add-margin-top"><b>Author(s)</b> &nbsp; Diaz-Guillen, J. A.; Fuentes, A. F.;
                        Diaz-Guillen, M. R.; et al.<br/><b>Source</b> &nbsp; Journal of Power Sources Volume: 186 Issue:
                        2 Pages: 349-352 Published: JAN 15 2009<br/><b>Times Cited</b> &nbsp; 31<br/><b>DOI</b> &nbsp;
                        10.1016/j.jpowsour.2008.09.106
                    </div>
                    <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=RID&amp;SrcApp=RID&amp;DestLinkType=FullRecord&amp;DestApp=ALL_WOS&amp;KeyUT=000262882400018"
                       class="btn btn-default btn-sm" target="_blank">Link gateway.webofknowledge.com</a>&nbsp;<a
                        href="http://dx.doi.org/10.1016/j.jpowsour.2008.09.106" class="btn btn-default btn-sm"
                        target="_blank">Link dx.doi.org</a>&nbsp;<a
                        href="../../files/publications/2009/DiazGuillenetalJPowerSources1862009p349.pdf"
                        class="btn btn-default" target="_blank">PDF</a></div>
            </div>
        </div>
    </div>



<?php require_once("inc/footer.html") ?>