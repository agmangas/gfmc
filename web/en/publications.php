<?php require_once "inc/header.html";?>
<?php require_once "inc/navigation.html";?>

<?php

$pubPages = array(
    array("name" => "1985-1990", "page" => "publ-1985-1990.php"),
    array("name" => "1991-1995", "page" => "publ-1991-1995.php"),
    array("name" => "1996-2000", "page" => "publ-1996-2000.php"),
    array("name" => "2001-2005", "page" => "publ-2001-2005.php"),
    array("name" => "2006", "page" => "publ-2006.php"),
    array("name" => "2007", "page" => "publ-2007.php"),
    array("name" => "2008", "page" => "publ-2008.php"),
    array("name" => "2009", "page" => "publ-2009.php"),
    array("name" => "2010", "page" => "publ-2010.php"),
    array("name" => "2011", "page" => "publ-2011.php"),
    array("name" => "2012", "page" => "publ-2012.php"),
    array("name" => "2013", "page" => "publ-2013.php"),
    array("name" => "2014", "page" => "publ-2014.php"),
    array("name" => "2015", "page" => "publ-2015.php"),
    array("name" => "2016", "page" => "publ-2016.php"),
    array("name" => "2017", "page" => "publ-2017.php"),
    array("name" => "2018", "page" => "publ-2018.php"),
);

?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>Publications</h1>
                    <hr class="small">
                    <span class="subheading">Physics of Complex Materials Group</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container publications-index">
    <div class="row col-md-6 col-md-offset-3">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="lead text-muted">Publications by year</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (array_reverse($pubPages) as $thePage): ?>
                <tr>
                    <td class="lead">
                        <a href="<?php echo $thePage["page"] ?>">
                            <?php echo $thePage["name"] ?>
                        </a>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

<?php require_once "inc/footer.html"?>