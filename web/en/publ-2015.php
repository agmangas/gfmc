<?php require_once "inc/header.html";?>
<?php require_once "inc/navigation.html";?>

<?php

$papers = array(
    array(
        "title" => "Microwave synthesis & sintering of Sm and Ca co-doped ceria ceramics",
        "fields" => array(
            "authors" => "J. Prado-Gonjal, R. Heuguet, D. Muñoz- Gil, A. Rivera-Calzada, S. Marinel, E. Morán, R. Schmidt",
            "source" => "International Journal of Hydrogen Energy 40, p.15640, 2015",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Anti-site disorder and physical properties in microwave synthesized RE2Ti2O7 (RE = Gd, Ho) pyrochlores",
        "fields" => array(
            "authors" => "A. Gómez-Pérez, J. Prado-Gonjal, E. Morán, M.T. Azcondo, U. Amador, R. Schmidt",
            "source" => "RSC Advances 5, p.85229, 2015",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Microwave-assisted routes for the synthesis of complex functional oxides",
        "fields" => array(
            "authors" => "J. Prado-Gonjal, E. Moran, R. Schmidt",
            "source" => "Inorganics 3, p.101, 2015",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Effect of sintering conditions on microstructure and dielectric properties of CaCu3Ti4O12 (CCTO) ceramics",
        "fields" => array(
            "authors" => "R. Löhnert, R. Schmidt, J. Töpfer",
            "source" => "Journal of Electroceramics 34, p.241, 2015",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Phase separation enhanced magneto-electric coupling in La0. 7Ca0. 3MnO3/BaTiO3 ultra-thin films",
        "fields" => array(
            "authors" => "A. Alberca, C. Munuera, J. Azpeitia, B. Kirby, N.M. Nemes, A.M. Perez-Muñoz, J. Tornos, F.J. Mompean, C. Leon, J. Santamaria, M. Garcia-Hernandez",
            "source" => "Scientific Reports, V5, 17926 2015.",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Record Seebeck coefficient and extremely low thermal conductivity in nanostructured SnSe",
        "fields" => array(
            "authors" => "F. Serrano-Sánchez, M. Gharsallah, N.M. Nemes, F.J. Mompean, J.L. Martínez, J.A. Alonso",
            "source" => "Applied Physics Letters, V106, 083902",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Charge density wave in layered La1−xCexSb2",
        "fields" => array(
            "authors" => "R.F. Luccas, A. Fente, J. Hanko, A. Correa-Orellana, E. Herrera, E. Climent-Pascual, J. Azpeitia, T. Pérez-Castañeda, M.R. Osorio, E. Salas-Colera, N.M. Nemes, F.J. Mompean, M. García-Hernández, J.G. Rodrigo, M.A. Ramos, I. Guillamón, S. Vieira, H. Suderow",
            "source" => "Physical Review B, V92, 235153",
        ),
        "links" => array(),
    ),
);

?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>Publications</h1>
                    <hr class="small">
                    <span class="subheading">2015</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?php foreach ($papers as $paper): ?>
            <div>
                <h4 class="paper-title">
                    <?php echo $paper["title"] ?>
                </h4>

                <div class="well add-margin-top">
                    <?php foreach ($paper["fields"] as $key => $val): ?>
                    <b>
                        <?php echo ucwords($key) ?>
                    </b> &nbsp;
                    <?php echo $val ?>
                    <br/>
                    <?php endforeach;?>
                </div>

                <?php foreach ($paper["links"] as $link): ?>
                <a href="<?php echo $link["url"] ?>" class="btn btn-default btn-sm" target="_blank">
                    Link
                    <?php echo $link["name"] ?>
                </a>
                <?php endforeach;?>
            </div>
            <hr/>
            <?php endforeach;?>

            <div>
                <h4 class="paper-title">
                    Phase separation enhanced magneto-electric coupling in La0.7Ca0.3MnO3/BaTiO3 ultra-thin films
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; A. Alberca, C. Munuera, J. Azpeitia, B. Kirby, N. M. Nemes, A. M. Perez-Muñoz, J.
                    Tornos, F. J. Mompean, C. Leon, J. Santamaria, M. Garcia-Hernandez
                    <br/>
                    <b>Source</b> &nbsp; Scientific Reports, accepted (2015)
                    <br/>
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    Paving the way to nanoionics: atomic origin of barriers for ionic transport through interfaces
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; M. A. Frechero , M. Rocci, G. Sánchez-Santolino, Amit Kumar, J. Salafranca, Rainer
                    Schmidt, M. R. Díaz-Guillén, O. J. Durá, A. Rivera-Calzada, R. Mishra, Stephen Jesse, S. T. Pantelides,
                    Sergei V. Kalinin, M. Varela, S. J. Pennycook, J. Santamaria y C. Leon
                    <br/>
                    <b>Source</b> &nbsp; Scientific Reports, accepted (2015)
                    <br/>
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    Formation of ​titanium monoxide (001) single-crystalline thin film induced by ion bombardment of ​titanium dioxide (110)
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; B.M. Pabón, J.I. Beltrán, G. Sánchez-Santolino, I. Palacio, J. López-Sánchez, J.
                    Rubio-Zuazo, J.M. Rojo, P. Ferrer, A. Mascaraque, M.C. Muñoz, M. Varela, G.R. Castro & O. Rodríguez de
                    la Fuente
                    <br/>
                    <b>Source</b> &nbsp; Nature Communications 6, Article number: 6147 doi:10.1038/ncomms7147
                    <br/>
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    Proximity Driven Commensurate Pinning in YBa2Cu3O7 through All-Oxide Magnetic Nanostructures
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; M. Rocci, J. Azpeitia, J. Trastoy, A. Perez-Muñoz, M. Cabero, R. F. Luccas, C. Munuera,
                    F. J. Mompean, M. Garcia-Hernandez, K. Bouzehouane, Z. Sefrioui, C. Leon, A. Rivera-Calzada, J.E. Villegas,
                    y J. Santamaria
                    <br/>
                    <b>Source</b> &nbsp; Nano Lett., accepted (2015). DOI: 10.1021/acs.nanolett.5b03261
                    <br/>
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    Magnetic field influence on the proximity effect at YBa2Cu3O7/La2/3Ca1/3MnO3 superconductor/half-metal interfaces
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; C. Visani, F. Cuellar, A. Perez-Munoz, Z. Sefrioui, C. León, J. Santamaría, and
                    J. E. Villegas
                    <br/>
                    <b>Source</b> &nbsp; Phys. Rev. B 92, 014519 (2015)
                    <br/>
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    Oxygen ion dynamics in pyrochlore-type ionic conductors: Effects of structure and ion–ion cooperativity
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; M. A. Frechero, O. J. Durá, M. R. Díaz-Guillén, K. J. Moreno, J. A. Díaz-Guillén,
                    J. García-Barriocanal, A. Rivera-Calzada, A. F. Fuentes, C. León
                    <br/>
                    <b>Source</b> &nbsp; J. Non-Cryst. Solids 407, 349 (2015)
                    <br/>
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    Insight into spin transport in oxide heterostructures from interface-resolved magnetic mapping
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; F. Y Bruno, M. N. Grisolia, C. Visani, S. Valencia, M. Varela, R. Abrudan, J. Tornos,
                    A. Rivera-Calzada, A. A. Ünal, S. J. Pennycook, Z. Sefrioui, C. Leon, J. E. Villegas, J. Santamaria,
                    A. Barthélémy and M. Bibes
                    <br/>
                    <b>Source</b> &nbsp; Nature Commun. 6:6306 (2015)
                    <br/>
                    <b>DOI</b> &nbsp; 10.1038/ncomms7306
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    Microstructure and electric properties of CaCu3Ti4O12 multilayer capacitors
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; R. Löhnert, H. Bartsch, R. Schmidt, B. Capraro, J. Töpfer
                    <br/>
                    <b>Source</b> &nbsp; Journal of the American Ceramic Society, 98, p.141 (2015)
                    <br/>
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    High pressure synthesis of Bi0.5Pb0.5CrO3 perovskite
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; I. Pirrotta, R. Schmidt, A.J. dos Santos, M. Garcia-Hernandez, E. Morán, M.A. Alario
                    <br/>
                    <b>Source</b> &nbsp; Journal of Solid State Chemistry 225, p.321 (2015)
                    <br/>
                </div>
            </div>
            <hr/>
            <div>
                <h4 class="paper-title">
                    Effect of sintering conditions on microstructure and dielectric properties of CaCu3Ti4O12 (CCTO) ceramics
                </h4>

                <div class="well add-margin-top">
                    <b>Author(s)</b> &nbsp; R. Löhnert, R. Schmidt, J. Töpfer
                    <br/>
                    <b>Source</b> &nbsp; Journal of Electroceramics, in Press
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once "inc/footer.html"?>