<?php require_once "inc/header.html";?>
<?php require_once "inc/navigation.html";?>

<?php

$papers = array(
    array(
        "title" => "Resonant electron tunnelling assisted by charged domain walls in multiferroic tunnel junctions",
        "fields" => array(
            "authors" => "G. Sanchez-Santolino, J. Tornos, D. Hernandez-Martin, J. I. Beltran,C. Munuera, M. Cabero, A. Perez-Muñoz, J. Ricote, F. Mompean, M. Garcia-Hernandez, Z. Sefrioui, C. Leon, S. J.Pennycook, M. C. Muñoz, M. Varela, J. Santamaria",
            "source" => "Nat Nano 12, 655 (2017). doi: 10.1038/nnano.2017.51",
        ),
        "links" => array(
            array("url" => "https://www.nature.com/articles/nnano.2017.51", "name" => "nature.com"),
        ),
    ),
    array(
        "title" => "Photodiodes based in La0.7Sr0.3MnO3 / single layer MoS2 hybrid vertical heterostructures",
        "fields" => array(
            "authors" => "Y. Niu, R. Frisenda, S. A. Svatek, G. Orfila, F. Gallego, P. Gant, N. Agrait, C. Leon, A. Rivera-Calzada, D. Perez de Lara, J. Santamaria y A. Castellanos-Gomez.",
            "source" => "2D Materials 4, 034002 (2017). doi: 10.1088/2053-1583/aa797b",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Modified magnetic anisotropy at LaCoO3/La0.7Sr0.3MnO3 interfaces",
        "fields" => array(
            "authors" => " M. Cabero, K. Nagy, F. Gallego, A. Sander, M. Rio, F.A. Cuellar, J.Tornos, D. Hernandez-Martin, N.M. Nemes, F. Mompean, M. Garcia-Hernandez, A. Rivera-Calzada, Z. Sefrioui, N. Reyren, T. Feher, M. Varela, C. Leon y J. Santamaria",
            "source" => "APL Materials 5, 096104 (2017). doi: 10.1063/1.5002090",
        ),
        "links" => array(),
    ),
    array(
        "title" => "3D elemental mapping with nanometer scale depth resolution via electron optical sectioning",
        "fields" => array(
            "authors" => "T. J. Pennycook, H. Yang, L. Jones, M. Cabero, A. Rivera-Calzada, C. Leon , M. Varela, J. Santamaria , P. D. Nellist.",
            "source" => "Ultramicroscopy 74, 27 (2017). doi: 10.1016/j.ultramic.2016.12.002.",
        ),
        "links" => array(),
    ),
    array(
        "title" => "In operando evidence of deoxygenation in ionic liquid gating of YBa2Cu3O7-x",
        "fields" => array(
            "authors" => "A. M. Perez-Muñoz, P. Schio, R. Poloni, A. Fernandez-Martinez, A. Rivera-Calzada, J. C. Cezar, E. Salas-Colera, G. R. Castro, J. Kinney, C. Leon, J. Santamaria, J. Garcia-Barriocanal, and A. M. Goldman.",
            "source" => "Proc. Natl. Acad. Sci. USA 114, 215 (2017). doi: 10.1073/pnas.1613006114.",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Thermodynamic modeling of nitrate materials for hybrid thermal energy storage: Using latent and sensible mechanisms",
        "fields" => array(
            "authors" => "J.I. Beltrán, J.Wang, F. Montero-Chacón and Y. Cui",
            "source" => "Solar Energy  155  Supplement C  154-166  2017. https://doi.org/10.1016/j.solener.2017.06.025",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Tunable correlated-electron phases in (111) LaAlO3/SrTiO3 band insulator heterostructures",
        "fields" => array(
            "authors" => "Juan I. Beltrán, and M.C. Muñoz",
            "source" => "Physical Review B  95  24  245120  2017",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Tuning the magnetic properties of pure hafnium by high pressure torsion",
        "fields" => array(
            "authors" => "C. M. Cepeda-Jiménez, J.I. Beltrán, A.Hernando,  M.A.García, F. Ynduráin, A. Zhilyaev and M. T. Pérez-Prado",
            "source" => "Acta Materialia  123  206-213  2017. http://dx.doi.org/10.1016/j.actamat.2016.10.052",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Unprecedented simultaneous enhancement in damage tolerance and fatigue resistance of zirconia/Ta composites",
        "fields" => array(
            "authors" => "A. Smirnov, J. I. Beltrán, T. Rodriguez-Suarez, C. Pecharromán, M. C. Muñoz, J. S.Moya & J. F. Bartolomé",
            "source" => "Scientific Reports  7  44922  2017. doi: 10.1038/srep44922",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Large magneto-electric coupling near room temperature in the high pressure ilmenite phase Mn2FeSbO6",
        "fields" => array(
            "authors" => "A. J. Dos santos-García, E. Solana-Madruga, C. Ritter, M. Garcia-Hernandez, R. Sáez-Puche, R. Schmidt",
            "source" => "Angewandte Chemie – International Edition, 56, p.4438, 2017",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Microwave-Assisted Synthesis, Microstructure and Magnetic Properties of Rare-Earth Cobaltites",
        "fields" => array(
            "authors" => "J. Gutiérrez Seijas, J. Prado-Gonjal, D. Ávila Brande, I. Terry, E. Morán, R. Schmidt",
            "source" => "Inorganic Chemistry 56, p.627, 2017",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Nanostructured discotic Pd(II) metallomesogens as one-dimensional proton conductors",
        "fields" => array(
            "authors" => "C. Cuerva, J.A. Campo, M. Cano, R. Schmidt",
            "source" => "Dalton Transactions 46, p.96, 2017",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Extra-low thermal conductivity in unfilled CoSb3-δ skutterudite synthesized under high-pressure conditions",
        "fields" => array(
            "authors" => "J Prado-Gonjal, F Serrano-Sánchez, NM Nemes, OJ Dura, JL Martínez, MT Fernández-Díaz, F Fauth, JA Alonso",
            "source" => "Applied Physics Letters 111 (8) 083902, 2017.",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Enhanced figure of merit in nanostructured (Bi, Sb)2Te3 with optimized composition, prepared by a straightforward arc-melting procedure",
        "fields" => array(
            "authors" => "F Serrano-Sánchez, M Gharsallah, NM Nemes, N Biskup, M Varela, JL Martínez, MT Fernández-Díaz, JA Alonso",
            "source" => "Scientific Reports 7 (1) 6277, 2017.",
        ),
        "links" => array(),
    ),
    array(
        "title" => "Effective anisotropies in magnetic nanowires using the torque method",
        "fields" => array(
            "authors" => "C Rotarescu, R Moreno, JA Fernández-Roldan, DG Trabada, NM Nemes, T Fehér, C Bran, M Vázquez, H Chiriac, N Lupu, T-A Óvári, O Chubykalo-Fesenko",
            "source" => "Journal of Magnetism and Magnetic Materials 443, 378-384, 2017.",
        ),
        "links" => array(),
    ),
);

?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('../../img/header-bg.jpg')">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <div class="page-heading">
          <h1>Publications</h1>
          <hr class="small">
          <span class="subheading">2017</span>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- Main Content -->
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <?php foreach ($papers as $paper): ?>
      <div>
        <h4 class="paper-title">
          <?php echo $paper["title"] ?>
        </h4>

        <div class="well add-margin-top">
          <?php foreach ($paper["fields"] as $key => $val): ?>
          <b><?php echo ucwords($key) ?></b> &nbsp;
          <?php echo $val ?>
          <br/>
          <?php endforeach;?>
        </div>

        <?php foreach ($paper["links"] as $link): ?>
        <a href="<?php echo $link["url"] ?>" class="btn btn-default btn-sm" target="_blank">
          Link
          <?php echo $link["name"] ?>
        </a>
        <?php endforeach;?>
      </div>
      <hr/>
      <?php endforeach;?>
    </div>
  </div>
</div>

<?php require_once "inc/footer.html"?>